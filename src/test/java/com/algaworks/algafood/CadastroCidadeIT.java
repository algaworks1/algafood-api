package com.algaworks.algafood;

import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.repository.CidadeRepository;
import com.algaworks.algafood.domain.repository.EstadoRepository;
import com.algaworks.algafood.security.NoAuthSecurityConfig;
import com.algaworks.algafood.util.DatabaseCleaner;
import com.algaworks.algafood.util.ResourceUtils;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringRunner.class)
@Import(NoAuthSecurityConfig.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/application-test.properties")
public class CadastroCidadeIT {

    private static final String VIOLACAO_DE_REGRA_DE_NEGOCIO_PROBLEM_TYPE = "Violação de regra de negócio";
    private static final String DADOS_INVALIDOS_PROBLEM_TITLE = "Dados inválidos";
    private static final int    CIDADE_ID_INEXISTENTE         = 100;

    @LocalServerPort
    private int port;

    @Autowired
    private DatabaseCleaner databaseCleaner;

    @Autowired
    private EstadoRepository estadoRepository;

    @Autowired
    private CidadeRepository cidadeRepository;
    private Cidade           jundiai;

    @Before
    public void setUp() {
	RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
	RestAssured.port = port;
	RestAssured.basePath = "/v1/cidades";
	databaseCleaner.clearTables();
	prepararDados();
    }

    @Test
    public void deveRetornarStatus200_QuandoConsultarCidades() {
	given()
	    .accept(ContentType.JSON)
	.when()
	    .get()
	.then()
	    .statusCode(HttpStatus.OK.value());
    }

    @Test
    public void deveRetornarStatus201_QuandoCadastrarCidade() {
	String jsonCidadeCorreta = ResourceUtils.getContentFromResource(
			"/json/correto/cidade-holambra.json");

	given()
	    .body(jsonCidadeCorreta)
	    .contentType(ContentType.JSON)
	    .accept(ContentType.JSON)
	.when()
	    .post()
	.then()
	    .statusCode(HttpStatus.CREATED.value());
    }

    @Test
    public void deveRetornarStatus400_QuandoCadastrarCidadeSemEstado() {
	String jsonCidadeSemEstado = ResourceUtils.getContentFromResource(
			"/json/incorreto/cidade-holambra-sem-estado.json");

	given()
	    .body(jsonCidadeSemEstado)
	    .contentType(ContentType.JSON)
	    .accept(ContentType.JSON)
	.when()
	    .post()
	.then()
	    .statusCode(HttpStatus.BAD_REQUEST.value())
	    .body("title", equalTo(DADOS_INVALIDOS_PROBLEM_TITLE));
    }

    @Test
    public void deveRetornarStatus400_QuandoCadastrarCidadeComEstadoInexistente() {
	String jsonCidadeComEstadoInexistente = ResourceUtils.getContentFromResource(
			"/json/incorreto/cidade-holambra-com-estado-inexistente.json");

	given()
	    .body(jsonCidadeComEstadoInexistente)
	    .contentType(ContentType.JSON)
	    .accept(ContentType.JSON)
	.when()
	    .post()
	.then()
	    .statusCode(HttpStatus.BAD_REQUEST.value())
	    .body("title", equalTo(VIOLACAO_DE_REGRA_DE_NEGOCIO_PROBLEM_TYPE));
    }

    @Test
    public void deveRetornarRespostaEStatusCorretos_QuandoConsultarCidadeExistente() {
	given()
	    .pathParam("restauranteId", jundiai.getId())
	    .accept(ContentType.JSON)
	.when()
	    .get("/{restauranteId}")
	.then()
	    .statusCode(HttpStatus.OK.value())
	    .body("nome", equalTo(jundiai.getNome()));
    }

    @Test
    public void deveRetornarStatus404_QuandoConsultarCidadeInexistente() {
	given()
	    .pathParam("restauranteId", CIDADE_ID_INEXISTENTE)
	    .accept(ContentType.JSON)
	.when()
	    .get("/{restauranteId}")
	.then()
	    .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void deveRetornarRespostaEStatusCorretos_QuandoAtualizarCidadeExistente() {
	String jsonRestauranteCorreto = ResourceUtils.getContentFromResource(
			"/json/correto/cidade-holambra.json");

	given()
	    .pathParam("restauranteId", jundiai.getId())
	    .body(jsonRestauranteCorreto)
	    .accept(ContentType.JSON)
	    .contentType(ContentType.JSON)
	.when()
	    .put("/{restauranteId}")
	.then()
	    .statusCode(HttpStatus.OK.value())
	    .body("nome", equalTo("Holambra"));
    }

    @Test
    public void deveRetornarStatus400_QuandoAtualizarCidadeInexistente() {
	String jsonRestauranteCorreto = ResourceUtils.getContentFromResource(
			"/json/correto/cidade-holambra.json");

	given()
	    .pathParam("restauranteId", CIDADE_ID_INEXISTENTE)
	    .body(jsonRestauranteCorreto)
	    .accept(ContentType.JSON)
	    .contentType(ContentType.JSON)
	.when()
	    .put("/{restauranteId}")
	.then()
	    .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void deveRetornarStatus204_QuandoExcluirCidadeExistente() {
	given()
	    .pathParam("cidadeId", jundiai.getId())
	.when()
	    .delete("/{cidadeId}")
	.then()
	    .statusCode(HttpStatus.NO_CONTENT.value());
    }

    @Test
    public void deveRetornarStatus404_QuandoExcluirCidadeInexistente() {
	given()
	    .pathParam("cidadeId", CIDADE_ID_INEXISTENTE)
	.when()
	    .delete("/{cidadeId}")
	.then()
	    .statusCode(HttpStatus.NOT_FOUND.value());
    }

    private void prepararDados() {
	Estado saoPaulo = new Estado();
	saoPaulo.setNome("São Paulo");
	estadoRepository.save(saoPaulo);

	Estado minasGerais = new Estado();
	minasGerais.setNome("Minas Gerais");
	estadoRepository.save(minasGerais);

	jundiai = new Cidade();
	jundiai.setNome("Jundiaí");
	jundiai.setEstado(saoPaulo);
	cidadeRepository.save(jundiai);

	Cidade camanducaia = new Cidade();
	camanducaia.setNome("Camanducaia");
	camanducaia.setEstado(minasGerais);
	cidadeRepository.save(camanducaia);
    }
	
}
