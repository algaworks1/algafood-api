package com.algaworks.algafood;

import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.CozinhaRepository;
import com.algaworks.algafood.domain.repository.RestauranteRepository;
import com.algaworks.algafood.security.NoAuthSecurityConfig;
import com.algaworks.algafood.util.DatabaseCleaner;
import com.algaworks.algafood.util.ResourceUtils;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import java.math.BigDecimal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

@RunWith(SpringRunner.class)
@Import(NoAuthSecurityConfig.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/application-test.properties")
public class CadastroCozinhaIT {

    private static final int COZINHA_ID_INEXISTENTE = 100;

    @LocalServerPort
    private int port;

    @Autowired
    private DatabaseCleaner databaseCleaner;

    @Autowired
    private CozinhaRepository cozinhaRepository;

    @Autowired
    private RestauranteRepository restauranteRepository;

    private Cozinha cozinhaAmericana;
    private int     quantidadeCozinhasCadastradas;

    @Before
    public void setUp() {
	RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
	RestAssured.port = port;
	RestAssured.basePath = "/v1/cozinhas";
	databaseCleaner.clearTables();
	prepararDados();
    }

    @Test
    public void deveRetornarStatus200_QuandoConsultarCozinhas() {
	given()
	    .accept(ContentType.JSON)
	.when()
	    .get()
	.then()
	    .statusCode(HttpStatus.OK.value());
    }

    @Test
    public void deveRetornarQuantidadeCorretaDeCozinhas_QuandoConsultarCozinhas() {
	given()
	    .accept(ContentType.JSON)
	.when()
	    .get()
	.then()
	    .body("_embedded.cozinhas", hasSize(quantidadeCozinhasCadastradas));
    }

    @Test
    public void deveRetornarStatus201_QuandoCadastrarCozinha() {
	String jsonCorretoCozinhaChinesa = ResourceUtils.getContentFromResource("/json/correto/cozinha-chinesa.json");

	given()
	    .body(jsonCorretoCozinhaChinesa)
	    .contentType(ContentType.JSON)
	    .accept(ContentType.JSON)
	.when()
	    .post()
	.then()
	    .statusCode(HttpStatus.CREATED.value());
    }

    @Test
    public void deveRetornarRespostaEStatusCorretos_QuandoConsultarCozinhaExistente() {
	given()
	    .pathParam("cozinhaId", cozinhaAmericana.getId())
	    .accept(ContentType.JSON)
	.when()
	    .get("/{cozinhaId}")
	.then()
	    .statusCode(HttpStatus.OK.value())
	    .body("nome", equalTo(cozinhaAmericana.getNome()));
    }

    @Test
    public void deveRetornarStatus404_QuandoConsultarCozinhaInexistente() {
	given()
	    .pathParam("cozinhaId", COZINHA_ID_INEXISTENTE)
	    .accept(ContentType.JSON)
	.when()
	    .get("/{cozinhaId}")
	.then()
	    .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void deveRetornarRespostaEStatusCorretos_QuandoAtualizarRestauranteExistente() {
	String jsonCorretoCozinhaChinesa = ResourceUtils.getContentFromResource("/json/correto/cozinha-chinesa.json");

	given()
	    .pathParam("restauranteId", cozinhaAmericana.getId())
	    .body(jsonCorretoCozinhaChinesa)
	    .accept(ContentType.JSON)
	    .contentType(ContentType.JSON)
	.when()
	    .put("/{restauranteId}")
	.then()
	    .statusCode(HttpStatus.OK.value())
	    .body("nome", equalTo("Chinesa"));
    }

    @Test
    public void deveRetornarStatus400_QuandoAtualizarRestauranteInexistente() {
	String jsonCorretoCozinhaChinesa = ResourceUtils.getContentFromResource("/json/correto/cozinha-chinesa.json");

	given()
	    .pathParam("restauranteId", COZINHA_ID_INEXISTENTE)
	    .body(jsonCorretoCozinhaChinesa)
	    .accept(ContentType.JSON)
	    .contentType(ContentType.JSON)
	.when()
	    .put("/{restauranteId}")
	.then()
	    .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void deveRetornarStatus204_QuandoExcluirCozinhaExistente() {
	given()
	    .pathParam("cozinhaId", cozinhaAmericana.getId())
	.when()
	    .delete("/{cozinhaId}")
	.then()
	    .statusCode(HttpStatus.NO_CONTENT.value());
    }

    @Test
    public void deveRetornarStatus404_QuandoExcluirCozinhaInexistente() {
	given()
	    .pathParam("cozinhaId", COZINHA_ID_INEXISTENTE)
	.when()
	    .delete("/{cozinhaId}")
	.then()
	    .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void deveRetornarStatus409_QuandoExcluirCozinhaEmUso() {
	Restaurante restauranteNewYork = new Restaurante();
	restauranteNewYork.setNome("New York");
	restauranteNewYork.setTaxaFrete(BigDecimal.ZERO);
        restauranteNewYork.setCozinha(cozinhaAmericana);

        restauranteRepository.save(restauranteNewYork);

	given()
	    .pathParam("cozinhaId", cozinhaAmericana.getId())
	.when()
	    .delete("/{cozinhaId}")
	.then()
	    .statusCode(HttpStatus.CONFLICT.value());
    }

    private void prepararDados() {
	Cozinha cozinhaTailandesa = new Cozinha();
	cozinhaTailandesa.setNome("Tailandesa");
	cozinhaRepository.save(cozinhaTailandesa);

	cozinhaAmericana = new Cozinha();
	cozinhaAmericana.setNome("Americana");
	cozinhaRepository.save(cozinhaAmericana);

	quantidadeCozinhasCadastradas = (int) cozinhaRepository.count();
    }
	
}
