package com.algaworks.algafood.security;

import com.algaworks.algafood.core.web.ProfileConstants;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@TestConfiguration
@EnableWebSecurity
@Profile(ProfileConstants.TEST)
public class NoAuthSecurityConfig
		extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
	// Disable CSRF
	httpSecurity.csrf().disable()
			// Permit all requests without authentication
			.authorizeRequests().anyRequest().permitAll();
    }

}
