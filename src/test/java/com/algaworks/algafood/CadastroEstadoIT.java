package com.algaworks.algafood;

import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.repository.CidadeRepository;
import com.algaworks.algafood.domain.repository.EstadoRepository;
import com.algaworks.algafood.security.NoAuthSecurityConfig;
import com.algaworks.algafood.util.DatabaseCleaner;
import com.algaworks.algafood.util.ResourceUtils;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

@RunWith(SpringRunner.class)
@Import(NoAuthSecurityConfig.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/application-test.properties")
public class CadastroEstadoIT {

    private static final int ESTADO_ID_INEXISTENTE = 100;

    @LocalServerPort
    private int port;

    @Autowired
    private DatabaseCleaner databaseCleaner;

    @Autowired
    private EstadoRepository estadoRepository;

    @Autowired
    private CidadeRepository cidadeRepository;

    private Estado pernambuco;
    private int    quantidadeEstadosCadastrados;

    @Before
    public void setUp() {
	RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
	RestAssured.port = port;
	RestAssured.basePath = "/v1/estados";
	databaseCleaner.clearTables();
	prepararDados();
    }

    @Test
    public void deveRetornarStatus200_QuandoConsultarEstados() {
	given()
	    .accept(ContentType.JSON)
	.when()
	    .get()
	.then()
	    .statusCode(HttpStatus.OK.value());
    }

    @Test
    public void deveRetornarQuantidadeCorretaDeEstados_QuandoConsultarEstados() {
	given()
	    .accept(ContentType.JSON)
	.when()
	    .get()
	.then()
	    .body("_embedded.estados", hasSize(quantidadeEstadosCadastrados));
    }

    @Test
    public void deveRetornarStatus201_QuandoCadastrarEstados() {
	String jsonCorretoEstadoSaoPaulo = ResourceUtils.getContentFromResource("/json/correto/estado-sao_paulo.json");

	given()
	    .body(jsonCorretoEstadoSaoPaulo)
	    .contentType(ContentType.JSON)
	    .accept(ContentType.JSON)
	.when()
	    .post()
	.then()
	    .statusCode(HttpStatus.CREATED.value());
    }

    @Test
    public void deveRetornarRespostaEStatusCorretos_QuandoConsultarEstadoExistente() {
	given()
	    .pathParam("estadoId", pernambuco.getId())
	    .accept(ContentType.JSON)
	.when()
	    .get("/{estadoId}")
	.then()
	    .statusCode(HttpStatus.OK.value())
	    .body("nome", equalTo(pernambuco.getNome()));
    }

    @Test
    public void deveRetornarStatus404_QuandoConsultarEstadoInexistente() {
	given()
	    .pathParam("estadoId", ESTADO_ID_INEXISTENTE)
	    .accept(ContentType.JSON)
	.when()
	    .get("/{estadoId}")
	.then()
	    .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void deveRetornarRespostaEStatusCorretos_QuandoAtualizarEstadoExistente() {
	String jsonCorretoEstadoSaoPaulo = ResourceUtils.getContentFromResource("/json/correto/estado-sao_paulo.json");

	given()
	    .pathParam("estadoId", pernambuco.getId())
	    .body(jsonCorretoEstadoSaoPaulo)
	    .accept(ContentType.JSON)
	    .contentType(ContentType.JSON)
	.when()
	    .put("/{estadoId}")
	.then()
	    .statusCode(HttpStatus.OK.value())
	    .body("nome", equalTo("São Paulo"));
    }

    @Test
    public void deveRetornarStatus400_QuandoAtualizarEstadoInexistente() {
	String jsonCorretoEstadoSaoPaulo = ResourceUtils.getContentFromResource("/json/correto/estado-sao_paulo.json");

	given()
	    .pathParam("estadoId", ESTADO_ID_INEXISTENTE)
	    .body(jsonCorretoEstadoSaoPaulo)
	    .accept(ContentType.JSON)
	    .contentType(ContentType.JSON)
	.when()
	    .put("/{estadoId}")
	.then()
	    .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void deveRetornarStatus204_QuandoExcluirEstadoExistente() {
	given()
	    .pathParam("estadoId", pernambuco.getId())
	.when()
	    .delete("/{estadoId}")
	.then()
	    .statusCode(HttpStatus.NO_CONTENT.value());
    }

    @Test
    public void deveRetornarStatus404_QuandoExcluirEstadoInexistente() {
	given()
	    .pathParam("estadoId", ESTADO_ID_INEXISTENTE)
	.when()
	    .delete("/{estadoId}")
	.then()
	    .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void deveRetornarStatus409_QuandoExcluirEstadoEmUso() {
	Cidade recife = new Cidade();
	recife.setNome("Recife");
	recife.setEstado(pernambuco);

        cidadeRepository.save(recife);

	given()
	    .pathParam("cozinhaId", pernambuco.getId())
	.when()
	    .delete("/{cozinhaId}")
	.then()
	    .statusCode(HttpStatus.CONFLICT.value());
    }

    private void prepararDados() {
	Estado parana = new Estado();
	parana.setNome("Parana");
	estadoRepository.save(parana);

	pernambuco = new Estado();
	pernambuco.setNome("Pernambuco");
	estadoRepository.save(pernambuco);

	quantidadeEstadosCadastrados = (int) estadoRepository.count();
    }
	
}
