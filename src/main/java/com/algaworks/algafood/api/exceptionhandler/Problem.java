package com.algaworks.algafood.api.exceptionhandler;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import java.util.List;
import lombok.Builder;
import lombok.Getter;

@ApiModel("Problema")
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Builder
public class Problem {

    private static final int POSITION_01 = 1;
    private static final int POSITION_05 = 5;
    private static final int POSITION_10 = 10;
    private static final int POSITION_15 = 15;
    private static final int POSITION_20 = 20;
    private static final int POSITION_25 = 25;
    private static final int POSITION_30 = 30;

    @ApiModelProperty(example = "400", position = POSITION_01)
    private Integer status;

    @ApiModelProperty(example = "2019-12-01T18:09:02.70844Z", position = POSITION_05)
    private OffsetDateTime timestamp;

    @ApiModelProperty(example = "https://algafood.com.br/dados-invalidos", position = POSITION_10)
    private String type;

    @ApiModelProperty(example = "Dados inválidos", position = POSITION_15)
    private String title;

    @ApiModelProperty(example = "Um ou mais campos estão inválidos. Faça o preenchimento correto e tente novamente.", position = POSITION_20)
    private String detail;

    @ApiModelProperty(example = "Um ou mais campos estão inválidos. Faça o preenchimento correto e tente novamente.", position = POSITION_25)
    private String userMessage;

    @ApiModelProperty(value = "Lista de objetos ou campos que geraram o erro (opcional)", position = POSITION_30)
    private List<Object> objects;

    @ApiModel("ObjetoProblema")
    @Getter
    @Builder
    public static class Object {

	@ApiModelProperty(example = "preco")
        private String name;

	@ApiModelProperty(example = "O preço é obrigatório")
        private String userMessage;
    }

}
