package com.algaworks.algafood.api.exceptionhandler;

import com.algaworks.algafood.core.validation.ValidacaoException;
import com.algaworks.algafood.domain.exception.EntidadeEmUsoException;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.PropertyBindingException;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.util.unit.DataSize;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static java.util.Optional.ofNullable;

@Slf4j
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String MSG_ERRO_GENERICA_USUARIO_FINAL = "Ocorreu um erro interno inesperado no sistema. Tente novamente e se "
		    + "o problema persistir, entre em contato com o administrador do sistema.";

    private final MessageSource messageSource;

    public ApiExceptionHandler(MessageSource messageSource) {
	this.messageSource = messageSource;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleGenericException(Exception ex, WebRequest request) {
	HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
	ProblemType problemType = ProblemType.ERRO_DE_SISTEMA;
	String detail = MSG_ERRO_GENERICA_USUARIO_FINAL;
	log.error(ex.getMessage(), ex);

	Problem problem = this.createProblemBuilder(httpStatus, problemType, detail)
			.userMessage(detail)
			.build();

	return this.handleExceptionInternal(ex, problem, HttpHeaders.EMPTY, httpStatus, request);
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus httpStatus, WebRequest request) {

        if(HttpStatus.NOT_FOUND == httpStatus){
            String detail = String.format("O recurso '%s', que você tentou acessar, é inexistente.", ex.getRequestURL());
	    Problem problem = this.createProblemBuilder(httpStatus, ProblemType.RECURSO_NAO_ENCONTRADO, detail)
			    .userMessage(MSG_ERRO_GENERICA_USUARIO_FINAL)
			    .build();

	    return this.handleExceptionInternal(ex, problem, HttpHeaders.EMPTY, httpStatus, request);
        }

	return super.handleNoHandlerFoundException(ex, headers, httpStatus, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers,
								  HttpStatus httpStatus, WebRequest request) {

	Throwable rootCause = ExceptionUtils.getRootCause(ex);

	if(rootCause instanceof InvalidFormatException) {
	    return this.handleInvalidFormatException((InvalidFormatException) rootCause, headers, httpStatus, request);
	}

	else if(rootCause instanceof PropertyBindingException) {
	    return this.handlePropertyBindingException((PropertyBindingException) rootCause, headers, httpStatus, request);
	}

        ProblemType problemType = ProblemType.MENSAGEM_INCOMPREENSIVEL;
	String detail = "O corpo da requisição está inválido. Verifique erro de sintaxe.";
	Problem problem = this.createProblemBuilder(httpStatus, problemType, detail)
			.userMessage(MSG_ERRO_GENERICA_USUARIO_FINAL)
			.build();

        return this.handleExceptionInternal(ex, problem, headers, httpStatus, request);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus httpStatus, WebRequest request) {

	if(ex instanceof MethodArgumentTypeMismatchException){
	    return handleMethodArgumentTypeMismatchException((MethodArgumentTypeMismatchException) ex, headers, httpStatus, request);
	}

	return super.handleTypeMismatch(ex, headers, httpStatus, request);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	return this.handleValidationInternal(ex, ex.getBindingResult(), headers, status, request);
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<?> handleMaxUploadSizeExceededException(MaxUploadSizeExceededException ex, WebRequest request) {
	HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
	ProblemType problemType = ProblemType.DADOS_INVALIDOS;

	String message = Optional.of(ex).map(MaxUploadSizeExceededException::getCause)
					.map(Throwable::getCause)
					.filter(FileSizeLimitExceededException.class::isInstance)
					.map(FileSizeLimitExceededException.class::cast)
					.map(FileSizeLimitExceededException::getPermittedSize)
					.map(maxSize -> String.format("Tamanho do arquivo excede o limite de %sMB.", DataSize.ofBytes(maxSize).toMegabytes()))
					.orElse(ex.getMessage());

	Problem problem = this.createProblemBuilder(httpStatus, problemType, message)
			.userMessage(message)
			.build();

	return this.handleExceptionInternal(ex, problem, HttpHeaders.EMPTY, httpStatus, request);
    }

    @ExceptionHandler(RecursoNaoEncontradoException.class)
    public ResponseEntity<?> handleExceptionRecursoNaoEncontradoException(RecursoNaoEncontradoException ex, WebRequest request) {
	HttpStatus httpStatus = HttpStatus.NOT_FOUND;
	ProblemType problemType = ProblemType.RECURSO_NAO_ENCONTRADO;
	String detail = ex.getMessage();

	Problem problem = this.createProblemBuilder(httpStatus, problemType, detail)
			.userMessage(detail)
			.build();

	return this.handleExceptionInternal(ex, problem, HttpHeaders.EMPTY, httpStatus, request);
    }

    @ExceptionHandler(EntidadeEmUsoException.class)
    public ResponseEntity<?> handleExceptionEntidadeEmUsoException(EntidadeEmUsoException ex, WebRequest request) {
	HttpStatus httpStatus = HttpStatus.CONFLICT;
	ProblemType problemType = ProblemType.ENTIDADE_EM_USO;
	String detail = ex.getMessage();

	Problem problem = this.createProblemBuilder(httpStatus, problemType, detail)
			.userMessage(detail)
			.build();

	return this.handleExceptionInternal(ex, problem, HttpHeaders.EMPTY, httpStatus, request);
    }

    @ExceptionHandler(NegocioException.class)
    public ResponseEntity<?> handleExceptionNegocioException(NegocioException ex, WebRequest request) {
	HttpStatus status = HttpStatus.BAD_REQUEST;
	ProblemType problemType = ProblemType.ERRO_NEGOCIO;
	String detail = ex.getMessage();

	Problem problem = this.createProblemBuilder(status, problemType, detail)
			.userMessage(detail)
			.build();

	return handleExceptionInternal(ex, problem, HttpHeaders.EMPTY, status, request);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> handleExceptionAccessDeniedException(AccessDeniedException ex, WebRequest request) {
	HttpStatus httpStatus = HttpStatus.FORBIDDEN;
	ProblemType problemType = ProblemType.ACESSO_NEGADO;
	String detail = ex.getMessage();
	var message = "Você não possui autorização para executar esta operação.";

	Problem problem = this.createProblemBuilder(httpStatus, problemType, detail)
			.userMessage(message)
			.build();

	return handleExceptionInternal(ex, problem, HttpHeaders.EMPTY, httpStatus, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
								  HttpStatus httpStatus, WebRequest request) {

	return this.handleValidationInternal(ex, ex.getBindingResult(), headers, httpStatus, request);
    }

    @ExceptionHandler(ValidacaoException.class)
    public ResponseEntity<?> handleExceptionValidacaoException(ValidacaoException ex, WebRequest request) {
        return this.handleValidationInternal(ex, ex.getBindingResult(), HttpHeaders.EMPTY, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return ResponseEntity.status(status).headers(headers).build();
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus httpStatus, WebRequest request) {

	if (body == null) {
	    body = Problem.builder()
			    .timestamp(OffsetDateTime.now())
			    .userMessage(MSG_ERRO_GENERICA_USUARIO_FINAL)
			    .title(httpStatus.getReasonPhrase())
			    .status(httpStatus.value())
			    .build();

	} else if (body instanceof String) {
	    body = Problem.builder()
			    .timestamp(OffsetDateTime.now())
			    .userMessage(MSG_ERRO_GENERICA_USUARIO_FINAL)
			    .title((String) body)
			    .status(httpStatus.value())
			    .build();
	}

	return super.handleExceptionInternal(ex, body, headers, httpStatus, request);
    }

    private ResponseEntity<Object> handleInvalidFormatException(InvalidFormatException ex, HttpHeaders headers, HttpStatus httpStatus, WebRequest request) {

	String path = ex.getPath().stream()
			.map(JsonMappingException.Reference::getFieldName)
			.collect(Collectors.joining("."));

	ProblemType problemType = ProblemType.MENSAGEM_INCOMPREENSIVEL;
	String detail = String.format("A propriedade '%s' recebeu o valor '%s', que é de um tipo inválido."
						      + " Corrija e informe um valor compatível com o tipo %s.", path, ex.getValue(), ex.getTargetType().getSimpleName());

	Problem problem = this.createProblemBuilder(httpStatus, problemType, detail)
			.userMessage(MSG_ERRO_GENERICA_USUARIO_FINAL)
			.build();

	return handleExceptionInternal(ex, problem, headers, httpStatus, request);
    }

    private ResponseEntity<Object> handlePropertyBindingException(PropertyBindingException ex, HttpHeaders headers, HttpStatus httpStatus, WebRequest request) {
	String propertyName = ex.getPath().stream()
			.map(JsonMappingException.Reference::getFieldName)
			.collect(Collectors.joining("."));

	String detail = String.format("A propriedade '%s' não existe. Corrija ou remova essa propriedade e tente novamente.", propertyName);
	ProblemType problemType = ProblemType.MENSAGEM_INCOMPREENSIVEL;
	Problem problem = this.createProblemBuilder(httpStatus, problemType, detail)
			.userMessage(MSG_ERRO_GENERICA_USUARIO_FINAL)
			.build();

	return this.handleExceptionInternal(ex, problem, headers, httpStatus, request);
    }

    private ResponseEntity<Object> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex, HttpHeaders headers, HttpStatus httpStatus, WebRequest request) {
	String pathVariableName = ex.getName();
	String value = (String) ex.getValue();
	String classRequired = ofNullable(ex.getRequiredType()).map(Class::getSimpleName).orElse("Classe não encontrada.");

	String detail = String.format("O parâmetro de URL '%s' recebeu o valor '%s', que é de um tipo inválido."
						      + " Corrija e informe um valor compatível com o tipo %s.", pathVariableName, value, classRequired);

	ProblemType problemType = ProblemType.PARAMETRO_INVALIDO;
	Problem problem = this.createProblemBuilder(httpStatus, problemType, detail)
			.userMessage(MSG_ERRO_GENERICA_USUARIO_FINAL)
			.build();

	return this.handleExceptionInternal(ex, problem, headers, httpStatus, request);
    }

    private ResponseEntity<Object> handleValidationInternal(Exception ex, BindingResult bindingResult, HttpHeaders headers,
							    HttpStatus status, WebRequest request) {

	ProblemType problemType = ProblemType.DADOS_INVALIDOS;
	String detail = "Um ou mais campos estão inválidos. Faça o preenchimento correto e tente novamente.";

	List<Problem.Object> problemObjects = bindingResult.getAllErrors().stream()
			.map(objectError -> {
			    String message = messageSource.getMessage(objectError, LocaleContextHolder.getLocale());

			    String name = objectError.getObjectName();

			    if (objectError instanceof FieldError) {
				name = ((FieldError) objectError).getField();
			    }

			    return Problem.Object.builder()
					    .name(name)
					    .userMessage(message)
					    .build();
			})
			.collect(Collectors.toList());

	Problem problem = createProblemBuilder(status, problemType, detail)
			.userMessage(detail)
			.objects(problemObjects)
			.build();

	return this.handleExceptionInternal(ex, problem, headers, status, request);
    }

    private Problem.ProblemBuilder createProblemBuilder(HttpStatus status, ProblemType problemType, String detail) {
	return Problem.builder()
			.timestamp(OffsetDateTime.now())
			.status(status.value())
			.type(problemType.getUri())
			.title(problemType.getTitle())
			.detail(detail);
    }
}
