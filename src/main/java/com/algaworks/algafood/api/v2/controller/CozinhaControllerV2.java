package com.algaworks.algafood.api.v2.controller;

import com.algaworks.algafood.api.ResourceUriHelper;
import com.algaworks.algafood.api.v2.assembler.CozinhaInputDisassemblerV2;
import com.algaworks.algafood.api.v2.assembler.CozinhaModelV2Assembler;
import com.algaworks.algafood.api.v2.model.CozinhaModelV2;
import com.algaworks.algafood.api.v2.model.input.CozinhaInputV2;
import com.algaworks.algafood.api.v2.openapi.controller.CozinhaControllerV2OpenApi;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.repository.CozinhaRepository;
import com.algaworks.algafood.domain.service.CadastroCozinhaService;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Data
@AllArgsConstructor
@RestController
@RequestMapping(path = "/v2/cozinhas", produces = MediaType.APPLICATION_JSON_VALUE)
public class CozinhaControllerV2 implements CozinhaControllerV2OpenApi {

    private final CozinhaRepository      cozinhaRepository;
    private final CadastroCozinhaService                    cadastroCozinhaService;
    private final CozinhaModelV2Assembler          modelAssembler;
    private final CozinhaInputDisassemblerV2       inputDisassembler;
    private final PagedResourcesAssembler<Cozinha> pagedResourcesAssembler;

    @GetMapping
    public PagedModel<CozinhaModelV2> listar(@PageableDefault(size = 2) Pageable pageable) {
	Page<Cozinha> cozinhasPaged = cozinhaRepository.findAll(pageable);
	return pagedResourcesAssembler.toModel(cozinhasPaged, modelAssembler);
    }

    @GetMapping("/{cozinhaId}")
    public CozinhaModelV2 buscar(@PathVariable Long cozinhaId) {
	Cozinha cozinha = this.cadastroCozinhaService.buscaPorId(cozinhaId);
	return modelAssembler.toModel(cozinha);
    }

    @PostMapping
    public CozinhaModelV2 adicionar(@RequestBody @Valid CozinhaInputV2 cozinhaInput) {
	Cozinha cozinha = inputDisassembler.toDomainObject(cozinhaInput);
	cadastroCozinhaService.salvar(cozinha);
	ResourceUriHelper.addUriInResponseHeader(cozinha.getId());
	return modelAssembler.toModel(cozinha);
    }

    @PutMapping("/{cozinhaId}")
    public CozinhaModelV2 atualizar(@PathVariable Long cozinhaId, @RequestBody @Valid CozinhaInputV2 cozinhaInput) {
        try {
	    Cozinha cozinha = cadastroCozinhaService.buscaPorId(cozinhaId);
	    inputDisassembler.copyToDomainObject(cozinhaInput, cozinha);
	    cadastroCozinhaService.salvar(cozinha);
	    return modelAssembler.toModel(cozinha);
	} catch (RecursoNaoEncontradoException e) {
            throw new NegocioException(e.getMessage(), e);
	}
    }

    @DeleteMapping("/{cozinhaId}")
    public ResponseEntity<?> remover(@PathVariable Long cozinhaId) {
        this.cadastroCozinhaService.excluir(cozinhaId);
        return ResponseEntity.noContent().build();
    }

}
