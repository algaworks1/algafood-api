package com.algaworks.algafood.api.v2.assembler;

import com.algaworks.algafood.api.v1.assembler.InputDisassembler;
import com.algaworks.algafood.api.v2.model.input.CidadeInputV2;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Estado;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CidadeInputV2Disassembler
		extends InputDisassembler<CidadeInputV2, Cidade> {

    public CidadeInputV2Disassembler(ModelMapper modelMapper) {
	super(modelMapper);
    }

    @Override
    public void copyToDomainObject(CidadeInputV2 cidadeInput, Cidade cidadeDestination) {
	// Para corrigir org.springframework.orm.jpa.JpaSystemException: identifier of an instance
	// of com.algaworks.algafood.domain.model.Cozinha was altered from 1 to 2
	cidadeDestination.setEstado(new Estado());
	super.copyToDomainObject(cidadeInput, cidadeDestination);
    }
}
