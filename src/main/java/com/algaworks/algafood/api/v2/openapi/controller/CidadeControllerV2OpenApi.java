package com.algaworks.algafood.api.v2.openapi.controller;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.v2.model.CidadeModelV2;
import com.algaworks.algafood.api.v2.model.input.CidadeInputV2;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CREATED;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;
import static org.apache.http.HttpStatus.SC_OK;

@Api(tags = "Cidades")
public interface CidadeControllerV2OpenApi {

    @ApiOperation("Lista as cidades")
    CollectionModel<CidadeModelV2> listar();

    @ApiOperation("Busca uma cidade por ID")
    @ApiResponses({@ApiResponse(code = SC_BAD_REQUEST, message = "ID da cidade inválido", response = Problem.class),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Cidade não encontrada", response = Problem.class)})
    CidadeModelV2 buscar(@ApiParam(value = "ID de uma cidade", example = "1", required = true) Long cidadeId);

    @ApiOperation("Cadastra uma cidade")
    @ApiResponses({@ApiResponse(code = SC_CREATED, message = "Cidade cadastrada"),})
    CidadeModelV2 adicionar(
		    @ApiParam(name = "corpo", value = "Representação de uma nova cidade", required = true) CidadeInputV2 cidadeInput);

    @ApiOperation("Atualiza uma cidade por ID")
    @ApiResponses({@ApiResponse(code = SC_OK, message = "Cidade atualizada"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Cidade não encontrada", response = Problem.class)})
    CidadeModelV2 atualizar(@ApiParam(value = "ID de uma cidade", example = "1", required = true) Long cidadeId,

			    @ApiParam(name = "corpo", value = "Representação de uma cidade com os novos dados", required = true) CidadeInputV2 cidadeInput);

    @ApiOperation("Exclui uma cidade por ID")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Cidade excluída"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Cidade não encontrada", response = Problem.class)})
    ResponseEntity<?> remover(@ApiParam(value = "ID de uma cidade", example = "1", required = true) Long cidadeId);

}
