package com.algaworks.algafood.api.v2.openapi.model;

import com.algaworks.algafood.api.v2.model.CozinhaModelV2;
import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.Links;

@ApiModel("CozinhasModel")
@Getter
@Setter
public class CozinhasModelV2OpenApi {

    private CozinhasV2EmbeddedModelApi _embedded;
    private Links                      _links;
    private PageModelV2OpenApi           page;

    @Getter
    @Setter
    @ApiModel("CozinhasEmbeddedModel")
    public class CozinhasV2EmbeddedModelApi {

	private List<CozinhaModelV2> cozinhas;
    }
}
