package com.algaworks.algafood.api.v2.controller;

import com.algaworks.algafood.api.ResourceUriHelper;
import com.algaworks.algafood.api.v2.assembler.CidadeInputV2Disassembler;
import com.algaworks.algafood.api.v2.assembler.CidadeModelV2Assembler;
import com.algaworks.algafood.api.v2.model.CidadeModelV2;
import com.algaworks.algafood.api.v2.model.input.CidadeInputV2;
import com.algaworks.algafood.api.v2.openapi.controller.CidadeControllerV2OpenApi;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.repository.CidadeRepository;
import com.algaworks.algafood.domain.service.CadastroCidadeService;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Data
@AllArgsConstructor
@RestController
@RequestMapping(path = "/v2/cidades", produces = MediaType.APPLICATION_JSON_VALUE)
public class CidadeControllerV2 implements CidadeControllerV2OpenApi {

    private final CidadeRepository                    cidadeRepository;
    private final CadastroCidadeService   cadastroCidadeService;
    private final CidadeModelV2Assembler    modelAssembler;
    private final CidadeInputV2Disassembler inputDisassembler;

    @GetMapping
    public CollectionModel<CidadeModelV2> listar() {
	List<Cidade> cidades = cidadeRepository.findAll();
	return modelAssembler.toCollectionModel(cidades);
    }

    @GetMapping("/{cidadeId}")
    public CidadeModelV2 buscar(@PathVariable Long cidadeId) {
	Cidade cidade = cadastroCidadeService.buscaPorId(cidadeId);
	return modelAssembler.toModel(cidade);
    }

    @PostMapping
    public CidadeModelV2 adicionar(@RequestBody @Valid CidadeInputV2 cidadeInput) {
	try {
	    Cidade cidade = inputDisassembler.toDomainObject(cidadeInput, Cidade.class);
	    cadastroCidadeService.salvar(cidade);
	    CidadeModelV2 cidadeModel = modelAssembler.toModel(cidade);
	    ResourceUriHelper.addUriInResponseHeader(cidadeModel.getIdCidade());
	    return cidadeModel;
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

    @PutMapping("/{cidadeId}")
    public CidadeModelV2 atualizar(@PathVariable Long cidadeId, @RequestBody @Valid CidadeInputV2 cidadeInput) {
	try {
	    Cidade cidade = cadastroCidadeService.buscaPorId(cidadeId);
	    inputDisassembler.copyToDomainObject(cidadeInput, cidade);
	    this.cadastroCidadeService.salvar(cidade);
	    return modelAssembler.toModel(cidade);
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}

    }

    @DeleteMapping("/{cidadeId}")
    public ResponseEntity<?> remover(@PathVariable Long cidadeId) {
	this.cadastroCidadeService.excluir(cidadeId);
	return ResponseEntity.noContent().build();
    }

}

