package com.algaworks.algafood.api.v2.openapi.model;

import com.algaworks.algafood.api.v2.model.CidadeModelV2;
import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Data;
import org.springframework.hateoas.Links;

@Data
@ApiModel("CidadesModel")
public class CidadesModelV2OpenApi {

    private CidadesV2EmbeddedModelApi _embedded;
    private Links                     _links;

    @Data
    @ApiModel("CidadesEmbeddedModel")
    public class CidadesV2EmbeddedModelApi {

	private List<CidadeModelV2> cidades;
    }
}
