package com.algaworks.algafood.api;

import java.net.URI;
import java.util.Optional;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

public final class ResourceUriHelper {

    private ResourceUriHelper() {}

    public static void addUriInResponseHeader(Object resourceId) {
	URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(resourceId).toUri();
	ServletRequestAttributes requestAttributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();

	Optional.ofNullable(requestAttributes).map(ServletRequestAttributes::getResponse).ifPresent(response -> {
	    response.setHeader(HttpHeaders.LOCATION, location.toString());
	    response.setStatus(HttpStatus.CREATED.value());
	});
    }
}
