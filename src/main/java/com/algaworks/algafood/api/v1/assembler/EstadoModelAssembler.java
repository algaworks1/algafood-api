package com.algaworks.algafood.api.v1.assembler;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.controller.EstadoController;
import com.algaworks.algafood.api.v1.model.EstadoModel;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.domain.model.Estado;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class EstadoModelAssembler
		extends RepresentationModelAssemblerSupport<Estado, EstadoModel> {

    private final ModelMapper modelMapper;
    private final AlgaLinks algaLinks;
    private final AlgaSecurity algaSecurity;

    public EstadoModelAssembler(ModelMapper modelMapper, AlgaLinks algaLinks, AlgaSecurity algaSecurity) {
	super(EstadoController.class, EstadoModel.class);
	this.modelMapper = modelMapper;
	this.algaLinks = algaLinks;
	this.algaSecurity = algaSecurity;
    }

    @Override
    public EstadoModel toModel(Estado estado) {
	EstadoModel estadoModel = createModelWithId(estado.getId(), estado);
	modelMapper.map(estado, estadoModel);

	if (algaSecurity.podeConsultarEstados()) {
	    estadoModel.add(algaLinks.linkToEstados("estados"));
	}

	return estadoModel;
    }

    @Override
    public CollectionModel<EstadoModel> toCollectionModel(Iterable<? extends Estado> estados) {
	CollectionModel<EstadoModel> collectionModel = super.toCollectionModel(estados);

	if (algaSecurity.podeConsultarEstados()) {
	    collectionModel.add(algaLinks.linkToEstados());
	}

	return collectionModel;
    }
}
