package com.algaworks.algafood.api.v1.openapi.controller;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.v1.model.GrupoModel;
import com.algaworks.algafood.api.v1.model.input.GrupoInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CREATED;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;
import static org.apache.http.HttpStatus.SC_OK;

@Api(tags = "Grupos")
public interface GrupoControllerOpenApi {

    @ApiOperation("Lista os grupos")
    CollectionModel<GrupoModel> listar();

    @ApiOperation("Busca um grupo por ID")
    @ApiResponses({@ApiResponse(code = SC_BAD_REQUEST, message = "ID da grupo inválido", response = Problem.class),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Grupo não encontrado", response = Problem.class)})
    GrupoModel buscar(@ApiParam(value = "ID de um grupo", example = "1", required = true) Long grupoId);

    @ApiOperation("Cadastra um grupo")
    @ApiResponses({@ApiResponse(code = SC_CREATED, message = "Grupo cadastrado"),})
    GrupoModel adicionar(@ApiParam(name = "corpo", value = "Representação de um novo grupo", required = true) GrupoInput grupoInput);

    @ApiOperation("Atualiza um grupo por ID")
    @ApiResponses({@ApiResponse(code = SC_OK, message = "Grupo atualizado"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Grupo não encontrado", response = Problem.class)})
    GrupoModel atualizar(@ApiParam(value = "ID de um grupo", example = "1", required = true) Long grupoId,
			 @ApiParam(name = "corpo", value = "Representação de um grupo com os novos dados", required = true) GrupoInput grupoInput);

    @ApiOperation("Exclui um grupo por ID")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Grupo excluído"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Grupo não encontrado", response = Problem.class)})
    ResponseEntity<?> remover(@ApiParam(value = "ID de um grupo", example = "1", required = true) Long grupoId);
}
