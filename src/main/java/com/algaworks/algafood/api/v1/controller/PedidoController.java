package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.v1.assembler.InputDisassembler;
import com.algaworks.algafood.api.v1.assembler.PedidoModelAssembler;
import com.algaworks.algafood.api.v1.assembler.PedidoResumoModelAssembler;
import com.algaworks.algafood.api.v1.model.PedidoModel;
import com.algaworks.algafood.api.v1.model.PedidoResumoModel;
import com.algaworks.algafood.api.v1.model.filter.PedidoFilter;
import com.algaworks.algafood.api.v1.model.input.PedidoInput;
import com.algaworks.algafood.api.v1.openapi.controller.PedidoControllerOpenApi;
import com.algaworks.algafood.core.data.PageWrapper;
import com.algaworks.algafood.core.data.PageableTranslator;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.model.Usuario;
import com.algaworks.algafood.domain.repository.PedidoRepository;
import com.algaworks.algafood.domain.service.EmissaoPedidoService;
import com.algaworks.algafood.infrastructure.repository.spec.PedidoSpecs;
import java.util.Map;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(path = "/v1/pedidos", produces = MediaType.APPLICATION_JSON_VALUE)
public class PedidoController implements PedidoControllerOpenApi {

    private static final int PAGE_SIZE_DEFAULT = 10;
    private final PedidoRepository pedidoRepository;
    private final EmissaoPedidoService                      emissaoPedidoService;
    private final PedidoModelAssembler                   pedidoModelAssembler;
    private final PedidoResumoModelAssembler             pedidoResumoModelAssembler;
    private final InputDisassembler<PedidoInput, Pedido> inputDisassembler;
    private final PagedResourcesAssembler<Pedido>        pagedResourcesAssembler;
    private final AlgaSecurity algaSecurity;

    @CheckSecurity.Pedidos.PodePesquisar
    @GetMapping
    @Override
    public PagedModel<PedidoResumoModel> pesquisar(PedidoFilter filter, @PageableDefault(size = PAGE_SIZE_DEFAULT) Pageable pageable) {
	Pageable pageableTraduzido = traduzirPageable(pageable);
        Page<Pedido> pedidosPaged = pedidoRepository.findAll(PedidoSpecs.usandoFiltro(filter), pageableTraduzido);
	pedidosPaged = new PageWrapper<>(pedidosPaged, pageable);
        return pagedResourcesAssembler.toModel(pedidosPaged, pedidoResumoModelAssembler);
    }

    @CheckSecurity.Pedidos.PodeBuscar
    @GetMapping("/{codigoPedido}")
    @Override
    public PedidoModel buscar(@PathVariable String codigoPedido) {
	Pedido pedido = emissaoPedidoService.buscaPorCodigo(codigoPedido);
	return pedidoModelAssembler.toModel(pedido);
    }

    @CheckSecurity.Pedidos.PodeCriar
    @PostMapping
    @Override
    public PedidoModel adicionar(@RequestBody @Valid PedidoInput pedidoInput) {
        try {
	    Pedido pedido = inputDisassembler.toDomainObject(pedidoInput, Pedido.class);

	    pedido.setCliente(new Usuario());
	    pedido.getCliente().setId(algaSecurity.getUsuarioId());

	    emissaoPedidoService.emitir(pedido);
	    return pedidoModelAssembler.toModel(pedido);
	} catch (RecursoNaoEncontradoException e) {
            throw new NegocioException(e.getMessage(), e);
	}
    }

    private Pageable traduzirPageable(Pageable apiPageable) {

	var mapeamento = Map.of(
			"codigo", "codigo",
			"restaurante.nome", "restaurante.nome",
			"nomeCliente", "cliente.nome",
			"valorTotal", "valorTotal"
			);

	return PageableTranslator.translate(apiPageable, mapeamento);
    }
}
