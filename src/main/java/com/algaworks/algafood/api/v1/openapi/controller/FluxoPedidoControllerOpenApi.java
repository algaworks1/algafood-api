package com.algaworks.algafood.api.v1.openapi.controller;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;

import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;

@Api(tags = "Pedidos")
public interface FluxoPedidoControllerOpenApi {

    @ApiOperation("Confirmação de pedido")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Pedido confirmado com sucesso"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Pedido não encontrado", response = Problem.class)})
    ResponseEntity<?> confirmar(@ApiParam(value = "Código do pedido", example = "f9981ca4-5a5e-4da3-af04-933861df3e55", required = true) String codigoPedido);

    @ApiOperation("Cancelamento de pedido")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Pedido cancelado com sucesso"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Pedido não encontrado", response = Problem.class)})
    ResponseEntity<?> cancelar(@ApiParam(value = "Código do pedido", example = "f9981ca4-5a5e-4da3-af04-933861df3e55", required = true) String codigoPedido);

    @ApiOperation("Registrar entrega de pedido")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Entrega de pedido registrada com sucesso"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Pedido não encontrado", response = Problem.class)})
    ResponseEntity<?> entregar(@ApiParam(value = "Código do pedido", example = "f9981ca4-5a5e-4da3-af04-933861df3e55", required = true) String codigoPedido);
}
