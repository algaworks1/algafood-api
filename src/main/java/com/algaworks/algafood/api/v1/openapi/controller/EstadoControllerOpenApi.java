package com.algaworks.algafood.api.v1.openapi.controller;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.v1.model.EstadoModel;
import com.algaworks.algafood.api.v1.model.input.EstadoInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CREATED;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;
import static org.apache.http.HttpStatus.SC_OK;

@Api(tags = "Estados")
public interface EstadoControllerOpenApi {

    @ApiOperation("Lista os estados")
    CollectionModel<EstadoModel> listar();

    @ApiOperation("Busca um estado por ID")
    @ApiResponses({@ApiResponse(code = SC_BAD_REQUEST, message = "ID do estado inválido", response = Problem.class),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Estado não encontrado", response = Problem.class)})
    EstadoModel buscar(@ApiParam(value = "ID de um estado", example = "1", required = true) Long estadoId);

    @ApiOperation("Cadastra um estado")
    @ApiResponses({@ApiResponse(code = SC_CREATED, message = "Estado cadastrado"),})
    EstadoModel adicionar(@ApiParam(name = "corpo", value = "Representação de um novo estado", required = true) EstadoInput estadoInput);

    @ApiOperation("Atualiza um estado por ID")
    @ApiResponses({@ApiResponse(code = SC_OK, message = "Estado atualizado"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Estado não encontrado", response = Problem.class)})
    EstadoModel atualizar(@ApiParam(value = "ID de um estado", example = "1", required = true) Long estadoId,
					  @ApiParam(name = "corpo", value = "Representação de um estado com os novos dados", required = true) EstadoInput estadoInput);

    @ApiOperation("Exclui um estado por ID")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Estado excluído"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Estado não encontrado", response = Problem.class)})
    ResponseEntity<?> remover(@ApiParam(value = "ID de um estado", example = "1", required = true) Long estadoId);
}
