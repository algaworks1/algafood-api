package com.algaworks.algafood.api.v1.assembler;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.controller.GrupoController;
import com.algaworks.algafood.api.v1.model.GrupoModel;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.domain.model.Grupo;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class GrupoModelAssembler
		extends RepresentationModelAssemblerSupport<Grupo, GrupoModel> {

    private final ModelMapper modelMapper;
    private final AlgaLinks algaLinks;
    private final AlgaSecurity algaSecurity;

    public GrupoModelAssembler(ModelMapper modelMapper, AlgaLinks algaLinks, AlgaSecurity algaSecurity) {
	super(GrupoController.class, GrupoModel.class);
	this.modelMapper = modelMapper;
	this.algaLinks = algaLinks;
	this.algaSecurity = algaSecurity;
    }

    @Override
    public GrupoModel toModel(Grupo grupo) {
	GrupoModel grupoModel = createModelWithId(grupo.getId(), grupo);
	modelMapper.map(grupo, grupoModel);

	if (algaSecurity.podeConsultarUsuariosGruposPermissoes()) {
	    grupoModel.add(algaLinks.linkToGrupos("grupos"));
	    grupoModel.add(algaLinks.linkToGrupoPermissoes(grupo.getId(),"permissoes"));
	}

	return grupoModel;
    }

    @Override
    public CollectionModel<GrupoModel> toCollectionModel(Iterable<? extends Grupo> grupos) {
	CollectionModel<GrupoModel> collectionModel = super.toCollectionModel(grupos);

	if (algaSecurity.podeConsultarUsuariosGruposPermissoes()) {
	    collectionModel.add(algaLinks.linkToGrupos());
	}

	return collectionModel;
    }
}
