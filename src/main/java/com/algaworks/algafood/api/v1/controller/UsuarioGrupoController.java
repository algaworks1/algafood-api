package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.assembler.GrupoModelAssembler;
import com.algaworks.algafood.api.v1.model.GrupoModel;
import com.algaworks.algafood.api.v1.openapi.controller.UsuarioGrupoControllerOpenApi;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Usuario;
import com.algaworks.algafood.domain.service.CadastroUsuarioService;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(path = "/v1/usuarios/{usuarioId}/grupos", produces = MediaType.APPLICATION_JSON_VALUE)
public class UsuarioGrupoController implements UsuarioGrupoControllerOpenApi {

    private final CadastroUsuarioService cadastroUsuarioService;
    private final GrupoModelAssembler    modelAssembler;
    private final AlgaLinks    algaLinks;
    private final AlgaSecurity algaSecurity;

    @CheckSecurity.UsuariosGruposPermissoes.PodeConsultar
    @GetMapping
    @Override
    public CollectionModel<GrupoModel> listar(@PathVariable Long usuarioId) {
	Usuario usuario = cadastroUsuarioService.buscaPorId(usuarioId);
	CollectionModel<GrupoModel> gruposModel = modelAssembler.toCollectionModel(usuario.getGrupos());

	if (algaSecurity.podeEditarUsuariosGruposPermissoes()) {
	    gruposModel.add(algaLinks.linkToUsuarioGrupoAssociacao(usuarioId, "associar"));

	    gruposModel.getContent().forEach(grupo -> {
		grupo.add(algaLinks.linkToUsuarioGrupoDesassociacao(usuarioId, grupo.getId(), "desassociar"));
	    });
	}

	return gruposModel;
    }

    @CheckSecurity.UsuariosGruposPermissoes.PodeEditar
    @PutMapping("/{grupoId}")
    @Override
    public ResponseEntity<?> associar(@PathVariable Long usuarioId, @PathVariable Long grupoId) {
	try {
	    cadastroUsuarioService.associarGrupo(usuarioId, grupoId);
	    return ResponseEntity.noContent().build();
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

    @CheckSecurity.UsuariosGruposPermissoes.PodeEditar
    @DeleteMapping("/{grupoId}")
    @Override
    public ResponseEntity<?> desassociar(@PathVariable Long usuarioId, @PathVariable Long grupoId) {
	try {
	    cadastroUsuarioService.desassociarGrupo(usuarioId, grupoId);
	    return ResponseEntity.noContent().build();
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

}
