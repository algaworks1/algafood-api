package com.algaworks.algafood.api.v1.openapi.controller;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.v1.model.UsuarioModel;
import com.algaworks.algafood.api.v1.model.input.SenhaInput;
import com.algaworks.algafood.api.v1.model.input.UsuarioComSenhaInput;
import com.algaworks.algafood.api.v1.model.input.UsuarioInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CREATED;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;
import static org.apache.http.HttpStatus.SC_OK;

@Api(tags = "Usuários")
public interface UsuarioControllerOpenApi {

    @ApiOperation("Lista os usuários")
    CollectionModel<UsuarioModel> listar();

    @ApiOperation("Busca um usuário por ID")
    @ApiResponses({@ApiResponse(code = SC_BAD_REQUEST, message = "ID do usuário inválido", response = Problem.class),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Usuário não encontrado", response = Problem.class)})
    UsuarioModel buscar(@ApiParam(value = "ID do usuário", example = "1", required = true) Long usuarioId);

    @ApiOperation("Cadastra um usuário")
    @ApiResponses({@ApiResponse(code = SC_CREATED, message = "Usuário cadastrado"),})
    UsuarioModel adicionar(
		    @ApiParam(name = "corpo", value = "Representação de um novo usuário", required = true) UsuarioComSenhaInput usuarioInput);

    @ApiOperation("Atualiza um usuário por ID")
    @ApiResponses({@ApiResponse(code = SC_OK, message = "Usuário atualizado"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Usuário não encontrado", response = Problem.class)})
    UsuarioModel atualizar(@ApiParam(value = "ID do usuário", example = "1", required = true) Long usuarioId,

					   @ApiParam(name = "corpo", value = "Representação de um usuário com os novos dados", required = true) UsuarioInput usuarioInput);

    @ApiOperation("Atualiza a senha de um usuário")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Senha alterada com sucesso"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Usuário não encontrado", response = Problem.class)})
    ResponseEntity<?> alterarSenha(@ApiParam(value = "ID do usuário", example = "1", required = true) Long usuarioId,
				   @ApiParam(name = "corpo", value = "Representação de uma nova senha", required = true) SenhaInput senha);
}
