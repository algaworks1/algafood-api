package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.ResourceUriHelper;
import com.algaworks.algafood.api.v1.assembler.GrupoModelAssembler;
import com.algaworks.algafood.api.v1.assembler.InputDisassembler;
import com.algaworks.algafood.api.v1.model.GrupoModel;
import com.algaworks.algafood.api.v1.model.input.GrupoInput;
import com.algaworks.algafood.api.v1.openapi.controller.GrupoControllerOpenApi;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Grupo;
import com.algaworks.algafood.domain.repository.GrupoRepository;
import com.algaworks.algafood.domain.service.CadastroGrupoService;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(path = "/v1/grupos", produces = MediaType.APPLICATION_JSON_VALUE)
public class GrupoController implements GrupoControllerOpenApi {

    private final GrupoRepository                      grupoRepository;
    private final CadastroGrupoService                 cadastroGrupo;
    private final GrupoModelAssembler                  modelAssembler;
    private final InputDisassembler<GrupoInput, Grupo> inputDisassembler;

    @CheckSecurity.UsuariosGruposPermissoes.PodeConsultar
    @GetMapping
    @Override
    public CollectionModel<GrupoModel> listar() {
	return modelAssembler.toCollectionModel(grupoRepository.findAll());
    }

    @CheckSecurity.UsuariosGruposPermissoes.PodeConsultar
    @GetMapping("/{grupoId}")
    @Override
    public GrupoModel buscar(@PathVariable Long grupoId) {
	Grupo grupo = cadastroGrupo.buscaPorId(grupoId);
	return modelAssembler.toModel(grupo);
    }

    @CheckSecurity.UsuariosGruposPermissoes.PodeEditar
    @PostMapping
    @Override
    public GrupoModel adicionar(@RequestBody @Valid GrupoInput grupoInput) {
	Grupo grupo = inputDisassembler.toDomainObject(grupoInput, Grupo.class);
	GrupoModel grupoModel = modelAssembler.toModel(cadastroGrupo.salvar(grupo));
	ResourceUriHelper.addUriInResponseHeader(grupoModel.getId());
	return grupoModel;
    }

    @CheckSecurity.UsuariosGruposPermissoes.PodeEditar
    @PutMapping("/{grupoId}")
    @Override
    public GrupoModel atualizar(@PathVariable Long grupoId, @RequestBody @Valid GrupoInput grupoInput) {
	try {
	    Grupo grupo = cadastroGrupo.buscaPorId(grupoId);
	    inputDisassembler.copyToDomainObject(grupoInput, grupo);
	    cadastroGrupo.salvar(grupo);
	    return modelAssembler.toModel(grupo);
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

    @CheckSecurity.UsuariosGruposPermissoes.PodeEditar
    @DeleteMapping("/{grupoId}")
    @Override
    public ResponseEntity<?> remover(@PathVariable Long grupoId) {
	cadastroGrupo.excluir(grupoId);
	return ResponseEntity.noContent().build();
    }

}
