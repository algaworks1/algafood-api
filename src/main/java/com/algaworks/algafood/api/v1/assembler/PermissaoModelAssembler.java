package com.algaworks.algafood.api.v1.assembler;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.model.PermissaoModel;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.domain.model.Permissao;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class PermissaoModelAssembler
		implements RepresentationModelAssembler<Permissao, PermissaoModel> {

    private final ModelMapper  modelMapper;
    private final AlgaLinks algaLinks;
    private final AlgaSecurity algaSecurity;

    public PermissaoModelAssembler(ModelMapper modelMapper, AlgaLinks algaLinks, AlgaSecurity algaSecurity) {
	this.modelMapper = modelMapper;
	this.algaLinks = algaLinks;
	this.algaSecurity = algaSecurity;
    }

    @Override
    public PermissaoModel toModel(Permissao permissao) {
	return modelMapper.map(permissao, PermissaoModel.class);
    }

    @Override
    public CollectionModel<PermissaoModel> toCollectionModel(Iterable<? extends Permissao> permissoes) {
	CollectionModel<PermissaoModel> collectionModel = RepresentationModelAssembler.super.toCollectionModel(permissoes);

	if (algaSecurity.podeConsultarUsuariosGruposPermissoes()) {
	    collectionModel.add(algaLinks.linkToPermissoes());
	}

	return collectionModel;
    }
}
