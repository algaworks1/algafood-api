package com.algaworks.algafood.api.v1.openapi.controller;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.v1.model.PermissaoModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;

@Api(tags = "Grupos")
public interface GrupoPermissaoControllerOpenApi {

    @ApiOperation("Lista as permissões associadas a um grupo")
    @ApiResponses({@ApiResponse(code = SC_BAD_REQUEST, message = "ID do grupo inválido", response = Problem.class),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Grupo não encontrado", response = Problem.class)})
    CollectionModel<PermissaoModel> listar(@ApiParam(value = "ID do grupo", example = "1", required = true) Long grupoId);

    @ApiOperation("Desassociação de permissão com grupo")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Desassociação realizada com sucesso"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Grupo ou permissão não encontrada", response = Problem.class)})
    ResponseEntity<?> desassociar(@ApiParam(value = "ID do grupo", example = "1", required = true) Long grupoId,
				  @ApiParam(value = "ID da permissão", example = "1", required = true) Long permissaoId);

    @ApiOperation("Associação de permissão com grupo")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Associação realizada com sucesso"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Grupo ou permissão não encontrada", response = Problem.class)})
    ResponseEntity<?> associar(@ApiParam(value = "ID do grupo", example = "1", required = true) Long grupoId,
			       @ApiParam(value = "ID da permissão", example = "1", required = true) Long permissaoId);
}
