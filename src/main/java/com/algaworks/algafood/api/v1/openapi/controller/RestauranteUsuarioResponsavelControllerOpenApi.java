package com.algaworks.algafood.api.v1.openapi.controller;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.v1.model.UsuarioModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;

import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;

@Api(tags = "Restaurantes")
public interface RestauranteUsuarioResponsavelControllerOpenApi {

    @ApiOperation("Lista os usuários responsáveis associados a restaurante")
    @ApiResponses({@ApiResponse(code = SC_NOT_FOUND, message = "Restaurante não encontrado", response = Problem.class)})
    CollectionModel<UsuarioModel> listar(@ApiParam(value = "ID do restaurante", example = "1", required = true) Long restauranteId);

    @ApiOperation("Desassociação de restaurante com usuário responsável")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Desassociação realizada com sucesso"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Restaurante ou usuário não encontrado", response = Problem.class)})
    ResponseEntity<?> desassociar(@ApiParam(value = "ID do restaurante", example = "1", required = true) Long restauranteId,

				  @ApiParam(value = "ID do usuário", example = "1", required = true) Long usuarioId);

    @ApiOperation("Associação de restaurante com usuário responsável")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Associação realizada com sucesso"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Restaurante ou usuário não encontrado", response = Problem.class)})
    ResponseEntity<?> associar(@ApiParam(value = "ID do restaurante", example = "1", required = true) Long restauranteId,
			       @ApiParam(value = "ID do usuário", example = "1", required = true) Long usuarioId);
}
