package com.algaworks.algafood.api.v1.openapi.model;

import com.algaworks.algafood.api.v1.model.PermissaoModel;
import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Data;
import org.springframework.hateoas.Links;

@Data
@ApiModel("PermissoesModel")
public class PermissoesModelOpenApi {

    private PermissoesEmbeddedModelApi _embedded;
    private Links                      _links;

    @Data
    @ApiModel("PermissoesEmbeddedModel")
    public class PermissoesEmbeddedModelApi {

	private List<PermissaoModel> permissoes;
    }

}
