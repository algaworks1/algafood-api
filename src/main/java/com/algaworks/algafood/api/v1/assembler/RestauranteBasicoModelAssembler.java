package com.algaworks.algafood.api.v1.assembler;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.controller.RestauranteController;
import com.algaworks.algafood.api.v1.model.RestauranteBasicoModel;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.domain.model.Restaurante;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class RestauranteBasicoModelAssembler extends RepresentationModelAssemblerSupport<Restaurante, RestauranteBasicoModel> {

    private final ModelMapper modelMapper;
    private final AlgaLinks    algaLinks;
    private final AlgaSecurity algaSecurity;

    public RestauranteBasicoModelAssembler(ModelMapper modelMapper, AlgaLinks algaLinks, AlgaSecurity algaSecurity) {
	super(RestauranteController.class, RestauranteBasicoModel.class);
	this.modelMapper = modelMapper;
	this.algaLinks = algaLinks;
	this.algaSecurity = algaSecurity;
    }

    @Override
    public RestauranteBasicoModel toModel(Restaurante restaurante) {
	RestauranteBasicoModel restauranteModel = createModelWithId(restaurante.getId(), restaurante);
	modelMapper.map(restaurante, restauranteModel);

	if (algaSecurity.podeConsultarRestaurantes()) {
	    restauranteModel.add(algaLinks.linkToRestaurantes("restaurantes"));
	}

	if (algaSecurity.podeConsultarCozinhas()) {
	    restauranteModel.getCozinha().add(algaLinks.linkToCozinha(restauranteModel.getCozinha().getId()));
	}

	return restauranteModel;
    }

    @Override
    public CollectionModel<RestauranteBasicoModel> toCollectionModel(Iterable<? extends Restaurante> restaurantes) {
	CollectionModel<RestauranteBasicoModel> collectionModel = super.toCollectionModel(restaurantes);

	if (algaSecurity.podeConsultarRestaurantes()) {
	    collectionModel.add(algaLinks.linkToRestaurantes());
	}

	return collectionModel;
    }
}
