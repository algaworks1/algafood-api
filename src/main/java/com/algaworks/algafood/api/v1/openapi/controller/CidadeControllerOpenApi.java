package com.algaworks.algafood.api.v1.openapi.controller;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.v1.model.CidadeModel;
import com.algaworks.algafood.api.v1.model.input.CidadeInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;

import static org.apache.http.HttpStatus.*;

@Api(tags = "Cidades")
public interface CidadeControllerOpenApi {

    @ApiOperation("Lista todas cidades")
    CollectionModel<CidadeModel> listar();

    @ApiOperation("Busca cidade por ID")
    @ApiResponses({@ApiResponse(code = SC_BAD_REQUEST, message = "ID da cidade inválido", response = Problem.class),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Cidade não encontrada", response = Problem.class)})
    CidadeModel buscar(@ApiParam(value = "ID de uma cidade", example = "1", required = true) Long cidadeId);

    @ApiOperation("Cadastra uma nova cidade")
    @ApiResponses({@ApiResponse(code = SC_CREATED, message = "Cidade cadastrada")})
    CidadeModel adicionar(@ApiParam(name = "corpo", value = "Representação de uma cidade", required = true) CidadeInput cidadeInput);

    @ApiOperation("Atualiza uma cidade por ID")
    @ApiResponses({@ApiResponse(code = SC_OK, message = "Cidade atualizada"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Cidade não encontrada", response = Problem.class)})
    ResponseEntity<CidadeModel> atualizar(@ApiParam(value = "ID de uma cidade", example = "1", required = true) Long cidadeId,
					  @ApiParam(name = "corpo", value = "Representação dos novos dados de uma cidade") CidadeInput cidadeInput);

    @ApiOperation("Exclui uma cidade por ID")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Cidade excluída"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Cidade não encontrada", response = Problem.class)})
    ResponseEntity<?> remover(@ApiParam(value = "ID de uma cidade", example = "1", required = true) Long cidadeId);

}
