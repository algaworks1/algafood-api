package com.algaworks.algafood.api.v1.assembler;

import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class InputDisassembler<I, O> {

    private ModelMapper modelMapper;

    public O toDomainObject(I objectInput, Class<O> outputTypeClass) {
	return modelMapper.map(objectInput, outputTypeClass);
    }

    public void copyToDomainObject(I source, O destination) {
	modelMapper.map(source, destination);
    }
}
