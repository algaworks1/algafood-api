package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.assembler.PermissaoModelAssembler;
import com.algaworks.algafood.api.v1.model.PermissaoModel;
import com.algaworks.algafood.api.v1.openapi.controller.GrupoPermissaoControllerOpenApi;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Grupo;
import com.algaworks.algafood.domain.service.CadastroGrupoService;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(path = "/v1/grupos/{grupoId}/permissoes", produces = MediaType.APPLICATION_JSON_VALUE)
public class GrupoPermissaoController implements GrupoPermissaoControllerOpenApi {

    private final CadastroGrupoService    cadastroGrupoService;
    private final PermissaoModelAssembler permissaoModelAssembler;
    private final AlgaLinks    algaLinks;
    private final AlgaSecurity algaSecurity;

    @CheckSecurity.UsuariosGruposPermissoes.PodeConsultar
    @GetMapping
    @Override
    public CollectionModel<PermissaoModel> listar(@PathVariable Long grupoId) {
	Grupo grupo = cadastroGrupoService.buscaPorId(grupoId);
	CollectionModel<PermissaoModel> permissoesModel = permissaoModelAssembler.toCollectionModel(grupo.getPermissoes()).removeLinks();

	permissoesModel.add(algaLinks.linkToGrupoPermissoes(grupoId));

	if (algaSecurity.podeEditarUsuariosGruposPermissoes()) {
	    permissoesModel.add(algaLinks.linkToGrupoPermissaoAssociacao(grupoId, "associar"));

	    permissoesModel.getContent().forEach(permissaoModel -> {
		permissaoModel.add(algaLinks.linkToGrupoPermissaoDesassociacao(grupoId, permissaoModel.getId(), "desassociar"));
	    });
	}

	return permissoesModel;
    }

    @CheckSecurity.UsuariosGruposPermissoes.PodeEditar
    @PutMapping("/{permissaoId}")
    @Override
    public ResponseEntity<?> associar(@PathVariable Long grupoId, @PathVariable Long permissaoId) {
	try {
	    cadastroGrupoService.associarPermissao(grupoId, permissaoId);
	    return ResponseEntity.noContent().build();
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

    @CheckSecurity.UsuariosGruposPermissoes.PodeEditar
    @DeleteMapping("/{permissaoId}")
    @Override
    public ResponseEntity<?> desassociar(@PathVariable Long grupoId, @PathVariable Long permissaoId) {
	try {
	    cadastroGrupoService.desassociarPermissao(grupoId, permissaoId);
	    return ResponseEntity.noContent().build();
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

}
