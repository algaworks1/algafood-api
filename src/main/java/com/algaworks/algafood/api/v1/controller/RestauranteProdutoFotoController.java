package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.v1.assembler.FotoProdutoModelAssembler;
import com.algaworks.algafood.api.v1.model.FotoProdutoModel;
import com.algaworks.algafood.api.v1.model.input.FotoProdutoInput;
import com.algaworks.algafood.api.v1.openapi.controller.RestauranteProdutoFotoControllerOpenApi;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.model.FotoProduto;
import com.algaworks.algafood.domain.model.Produto;
import com.algaworks.algafood.domain.service.CadastroProdutoService;
import com.algaworks.algafood.domain.service.CatalogoFotoProdutoService;
import com.algaworks.algafood.domain.service.StorageFotoService.FotoRecuperada;
import java.io.IOException;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@AllArgsConstructor
@RestController
@RequestMapping(path = "/v1/restaurantes/{restauranteId}/produtos/{produtoId}/foto", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestauranteProdutoFotoController implements RestauranteProdutoFotoControllerOpenApi {

    private final CadastroProdutoService cadastroProdutoService;
    private final CatalogoFotoProdutoService catalogoFotoProdutoService;
    private final FotoProdutoModelAssembler  modelAssembler;

    @CheckSecurity.Restaurantes.PodeGerenciarFuncionamento
    @PutMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Override
    public ResponseEntity<FotoProdutoModel> atualizarFoto(@PathVariable Long restauranteId, @PathVariable Long produtoId,
							  @Valid FotoProdutoInput fotoProdutoInput, @RequestPart MultipartFile arquivo) throws IOException {

	Produto produto = cadastroProdutoService.buscaPorId(restauranteId, produtoId);

	FotoProduto fotoProduto = new FotoProduto();
	fotoProduto.setProduto(produto);
	fotoProduto.setContentType(arquivo.getContentType());
	fotoProduto.setNomeArquivo(arquivo.getOriginalFilename());
	fotoProduto.setTamanho(arquivo.getSize());
	fotoProduto.setDescricao(fotoProdutoInput.getDescricao());

	fotoProduto = catalogoFotoProdutoService.salvar(fotoProduto, arquivo.getInputStream());

	FotoProdutoModel fotoProdutoModel = modelAssembler.toModel(fotoProduto);
	return ResponseEntity.ok(fotoProdutoModel);
    }

    @CheckSecurity.Restaurantes.PodeConsultar
    @GetMapping
    @Override
    public ResponseEntity<FotoProdutoModel> buscar(@PathVariable Long restauranteId, @PathVariable Long produtoId) {
	FotoProduto fotoProduto = catalogoFotoProdutoService.buscarOuFalhar(restauranteId, produtoId);
	FotoProdutoModel fotoProdutoModel = modelAssembler.toModel(fotoProduto);
	return ResponseEntity.ok(fotoProdutoModel);
    }

    @GetMapping(produces = MediaType.ALL_VALUE)
    @Override
    public ResponseEntity<InputStreamResource> servir(@PathVariable Long restauranteId, @PathVariable Long produtoId,
						      @RequestHeader(name = "accept") String acceptHeader) throws HttpMediaTypeNotAcceptableException {
	try {
	    FotoProduto fotoProduto = catalogoFotoProdutoService.buscarOuFalhar(restauranteId, produtoId);

	    MediaType mediaTypeFoto = MediaType.parseMediaType(fotoProduto.getContentType());
	    List<MediaType> acceptedMediaTypes = MediaType.parseMediaTypes(acceptHeader);
	    verificaCompatibilidadeMediaType(mediaTypeFoto, acceptedMediaTypes);

	    FotoRecuperada fotoRecuperada = catalogoFotoProdutoService.recuperar(fotoProduto.getNomeArquivo());

	    if (fotoRecuperada.temInputStream()) {
		return ResponseEntity.ok()
				.contentType(mediaTypeFoto)
				.body(new InputStreamResource(fotoRecuperada.getInputStream()));
	    }

	    return ResponseEntity.status(HttpStatus.FOUND)
			    .header(HttpHeaders.LOCATION, fotoRecuperada.getUrl())
			    .build();
	} catch (RecursoNaoEncontradoException e) {
	    return ResponseEntity.notFound().build();
	}
    }

    @CheckSecurity.Restaurantes.PodeGerenciarFuncionamento
    @DeleteMapping
    @Override
    public ResponseEntity<?> excluir(@PathVariable Long restauranteId, @PathVariable Long produtoId) {
	catalogoFotoProdutoService.excluir(restauranteId, produtoId);
	return ResponseEntity.noContent().build();
    }

    private void verificaCompatibilidadeMediaType(MediaType mediaTypeFoto, List<MediaType> acceptedMediaTypes) throws HttpMediaTypeNotAcceptableException {
	boolean isCompativel = acceptedMediaTypes.stream().anyMatch(mediaType -> mediaType.isCompatibleWith(mediaTypeFoto));
	if(!isCompativel) {
	    throw new HttpMediaTypeNotAcceptableException(acceptedMediaTypes);
	}
    }
}
