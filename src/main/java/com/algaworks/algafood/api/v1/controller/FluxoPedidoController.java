package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.v1.openapi.controller.FluxoPedidoControllerOpenApi;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.PedidoNaoEncontradoException;
import com.algaworks.algafood.domain.service.FluxoPedidoService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RequestMapping(path = "/v1/pedidos/{codigoPedido}", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class FluxoPedidoController implements FluxoPedidoControllerOpenApi {

    private final FluxoPedidoService fluxoPedidoService;

    @CheckSecurity.Pedidos.PodeGerenciar
    @PutMapping("/confirmacao")
    @Override
    public ResponseEntity<?> confirmar(@PathVariable String codigoPedido) {
	try {
	    fluxoPedidoService.confirmar(codigoPedido);
	    return ResponseEntity.noContent().build();
	} catch (PedidoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

    @CheckSecurity.Pedidos.PodeGerenciar
    @PutMapping("/entrega")
    @Override
    public ResponseEntity<?> entregar(@PathVariable String codigoPedido) {
	try {
	    fluxoPedidoService.entregar(codigoPedido);
	    return ResponseEntity.noContent().build();
	} catch (PedidoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

    @CheckSecurity.Pedidos.PodeGerenciar
    @PutMapping("/cancelamento")
    @Override
    public ResponseEntity<?> cancelar(@PathVariable String codigoPedido) {
	try {
	    fluxoPedidoService.cancelar(codigoPedido);
	    return ResponseEntity.noContent().build();
	} catch (PedidoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

}
