package com.algaworks.algafood.api.v1.assembler;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.controller.RestauranteController;
import com.algaworks.algafood.api.v1.model.RestauranteApenasNomeModel;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.domain.model.Restaurante;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class RestauranteApenasNomeModelAssembler extends RepresentationModelAssemblerSupport<Restaurante, RestauranteApenasNomeModel> {

    private final ModelMapper modelMapper;
    private final AlgaLinks    algaLinks;
    private final AlgaSecurity algaSecurity;

    public RestauranteApenasNomeModelAssembler(ModelMapper modelMapper, AlgaLinks algaLinks, AlgaSecurity algaSecurity) {
        super(RestauranteController.class, RestauranteApenasNomeModel.class);
	this.modelMapper = modelMapper;
	this.algaLinks = algaLinks;
	this.algaSecurity = algaSecurity;
    }

    @Override
    public RestauranteApenasNomeModel toModel(Restaurante restaurante) {
	RestauranteApenasNomeModel restauranteModel = createModelWithId(restaurante.getId(), restaurante);
	modelMapper.map(restaurante, restauranteModel);

	if (algaSecurity.podeConsultarRestaurantes()) {
	    restauranteModel.add(algaLinks.linkToRestaurantes("restaurantes"));
	}

	return restauranteModel;
    }

    @Override
    public CollectionModel<RestauranteApenasNomeModel> toCollectionModel(Iterable<? extends Restaurante> restaurantes) {
	CollectionModel<RestauranteApenasNomeModel> collectionModel = super.toCollectionModel(restaurantes);

	if (algaSecurity.podeConsultarRestaurantes()) {
	    collectionModel.add(algaLinks.linkToRestaurantes());
	}

	return collectionModel;
    }
}
