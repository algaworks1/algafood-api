package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.v1.assembler.FormaPagamentoModelAssembler;
import com.algaworks.algafood.api.v1.assembler.InputDisassembler;
import com.algaworks.algafood.api.v1.model.FormaPagamentoModel;
import com.algaworks.algafood.api.v1.model.input.FormaPagamentoInput;
import com.algaworks.algafood.api.v1.openapi.controller.FormaPagamentoControllerOpenApi;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.model.FormaPagamento;
import com.algaworks.algafood.domain.repository.FormaPagamentoRepository;
import com.algaworks.algafood.domain.service.CadastroFormaPagamentoService;
import java.net.URI;
import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.filter.ShallowEtagHeaderFilter;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@AllArgsConstructor
@RestController
@RequestMapping(path = "/v1/formas-pagamento", produces = MediaType.APPLICATION_JSON_VALUE)
public class FormaPagamentoController implements FormaPagamentoControllerOpenApi {

    private static final int MAX_AGE = 10;

    private final FormaPagamentoRepository formaPagamentoRepository;
    private final CadastroFormaPagamentoService cadastroFormaPagamentoService;
    private final FormaPagamentoModelAssembler modelAssembler;
    private final InputDisassembler<FormaPagamentoInput, FormaPagamento> inputDisassembler;

    @CheckSecurity.FormasPagamento.PodeConsultar
    @GetMapping
    @Override
    public ResponseEntity<CollectionModel<FormaPagamentoModel>> listar(ServletWebRequest request) {
	ShallowEtagHeaderFilter.disableContentCaching(request.getRequest());

	Optional<OffsetDateTime> ultimaAtualizacaoOpt = formaPagamentoRepository.getDataUltimaAtualizacao();
	String eTag = ultimaAtualizacaoOpt.map(OffsetDateTime::toEpochSecond).map(String::valueOf).orElse("0");

	if (request.checkNotModified(eTag)) {
	    return null;
	}

	CollectionModel<FormaPagamentoModel> formaPagamentoModels = modelAssembler.toCollectionModel(formaPagamentoRepository.findAll());

	return ResponseEntity.ok()
			.cacheControl(CacheControl.maxAge(MAX_AGE, TimeUnit.SECONDS).cachePublic())
			.body(formaPagamentoModels);
    }

    @CheckSecurity.FormasPagamento.PodeConsultar
    @GetMapping("/{formaPagamentoId}")
    @Override
    public ResponseEntity<FormaPagamentoModel> buscar(@PathVariable Long formaPagamentoId, ServletWebRequest request) {
	FormaPagamento formaPagamento = this.cadastroFormaPagamentoService.buscaPorId(formaPagamentoId);

	Optional<OffsetDateTime> ultimaAtualizacaoOpt = formaPagamentoRepository.getDataUltimaAtualizacao();
	String eTag = ultimaAtualizacaoOpt.map(OffsetDateTime::toEpochSecond).map(String::valueOf).orElse("0");

	if (request.checkNotModified(eTag)) {
	    return null;
	}

	FormaPagamentoModel formaPagamentoModel = modelAssembler.toModel(formaPagamento);

	return ResponseEntity.ok()
			.cacheControl(CacheControl.maxAge(MAX_AGE, TimeUnit.SECONDS).cachePublic())
			.body(formaPagamentoModel);
    }

    @CheckSecurity.FormasPagamento.PodeEditar
    @PostMapping
    @Override
    public ResponseEntity<FormaPagamentoModel> adicionar(@RequestBody @Valid FormaPagamentoInput formaPagamentoInput) {
	FormaPagamento formaPagamento = inputDisassembler.toDomainObject(formaPagamentoInput, FormaPagamento.class);
	cadastroFormaPagamentoService.salvar(formaPagamento);
	URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(formaPagamento.getId()).toUri();
	return ResponseEntity.created(location).body(modelAssembler.toModel(formaPagamento));
    }

    @CheckSecurity.FormasPagamento.PodeEditar
    @PutMapping("/{formaPagamentoId}")
    @Override
    public ResponseEntity<FormaPagamentoModel> atualizar(@PathVariable Long formaPagamentoId, @RequestBody @Valid FormaPagamentoInput formaPagamentoInput) {
	try {
	    FormaPagamento formaPagamento = cadastroFormaPagamentoService.buscaPorId(formaPagamentoId);
	    inputDisassembler.copyToDomainObject(formaPagamentoInput, formaPagamento);
	    cadastroFormaPagamentoService.salvar(formaPagamento);
	    return ResponseEntity.ok(modelAssembler.toModel(formaPagamento));
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

    @DeleteMapping("/{formaPagamentoId}")
    @Override
    public ResponseEntity<?> remover(@PathVariable Long formaPagamentoId) {
	cadastroFormaPagamentoService.excluir(formaPagamentoId);
	return ResponseEntity.noContent().build();
    }

}
