package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.ResourceUriHelper;
import com.algaworks.algafood.api.v1.assembler.CozinhaModelAssembler;
import com.algaworks.algafood.api.v1.assembler.InputDisassembler;
import com.algaworks.algafood.api.v1.model.CozinhaModel;
import com.algaworks.algafood.api.v1.model.input.CozinhaInput;
import com.algaworks.algafood.api.v1.openapi.controller.CozinhaControllerOpenApi;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.repository.CozinhaRepository;
import com.algaworks.algafood.domain.service.CadastroCozinhaService;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Data
@AllArgsConstructor
@RestController
@RequestMapping(path = "/v1/cozinhas", produces = MediaType.APPLICATION_JSON_VALUE)
public class CozinhaController implements CozinhaControllerOpenApi {

    private final CozinhaRepository cozinhaRepository;
    private final CadastroCozinhaService cadastroCozinhaService;
    private final CozinhaModelAssembler modelAssembler;
    private final InputDisassembler<CozinhaInput,Cozinha> inputDisassembler;
    private final PagedResourcesAssembler<Cozinha> pagedResourcesAssembler;

    @CheckSecurity.Cozinhas.PodeConsultar
    @GetMapping
    @Override
    public PagedModel<CozinhaModel> listar(@PageableDefault(size = 2) Pageable pageable) {
	Page<Cozinha> cozinhasPaged = cozinhaRepository.findAll(pageable);
	return pagedResourcesAssembler.toModel(cozinhasPaged, modelAssembler);
    }

    @CheckSecurity.Cozinhas.PodeConsultar
    @GetMapping("/{cozinhaId}")
    @Override
    public CozinhaModel buscar(@PathVariable Long cozinhaId) {
	Cozinha cozinha = this.cadastroCozinhaService.buscaPorId(cozinhaId);
	return modelAssembler.toModel(cozinha);
    }

    @CheckSecurity.Cozinhas.PodeEditar
    @PostMapping
    @Override
    public CozinhaModel adicionar(@RequestBody @Valid CozinhaInput cozinhaInput) {
	Cozinha cozinha = inputDisassembler.toDomainObject(cozinhaInput, Cozinha.class);
	cadastroCozinhaService.salvar(cozinha);
	ResourceUriHelper.addUriInResponseHeader(cozinha.getId());
	return modelAssembler.toModel(cozinha);
    }

    @CheckSecurity.Cozinhas.PodeEditar
    @PutMapping("/{cozinhaId}")
    @Override
    public CozinhaModel atualizar(@PathVariable Long cozinhaId, @RequestBody @Valid CozinhaInput cozinhaInput) {
        try {
	    Cozinha cozinha = cadastroCozinhaService.buscaPorId(cozinhaId);
	    inputDisassembler.copyToDomainObject(cozinhaInput, cozinha);
	    cadastroCozinhaService.salvar(cozinha);
	    return modelAssembler.toModel(cozinha);
	} catch (RecursoNaoEncontradoException e) {
            throw new NegocioException(e.getMessage(), e);
	}
    }

    @CheckSecurity.Cozinhas.PodeEditar
    @DeleteMapping("/{cozinhaId}")
    @Override
    public ResponseEntity<?> remover(@PathVariable Long cozinhaId) {
        this.cadastroCozinhaService.excluir(cozinhaId);
        return ResponseEntity.noContent().build();
    }

}
