package com.algaworks.algafood.api.v1.openapi.model;

import com.algaworks.algafood.api.v1.model.GrupoModel;
import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Data;
import org.springframework.hateoas.Links;

@Data
@ApiModel("GruposModel")
public class GruposModelOpenApi {

    private GruposEmbeddedModelApi _embedded;
    private Links                  _links;

    @Data
    @ApiModel("GruposEmbeddedModel")
    public class GruposEmbeddedModelApi {

	private List<GrupoModel> grupos;
    }
}
