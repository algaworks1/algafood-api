package com.algaworks.algafood.api.v1.openapi.controller;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.v1.model.FotoProdutoModel;
import com.algaworks.algafood.api.v1.model.input.FotoProdutoInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.multipart.MultipartFile;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;
import static org.apache.http.HttpStatus.SC_OK;

@Api(tags = "Produtos")
public interface RestauranteProdutoFotoControllerOpenApi {

    @ApiOperation("Atualiza a foto do produto de um restaurante")
    @ApiResponses({@ApiResponse(code = SC_OK, message = "Foto do produto atualizada"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Produto de restaurante não encontrado", response = Problem.class)})
    ResponseEntity<FotoProdutoModel> atualizarFoto(@ApiParam(value = "ID do restaurante", example = "1", required = true) Long restauranteId,
						   @ApiParam(value = "ID do produto", example = "1", required = true) Long produtoId,
						   FotoProdutoInput fotoProdutoInput,
						   @ApiParam(value = "costela-suina.jpg", example = "Arquivo da foto do produto (máximo 500KB, apenas JPG e PNG)", required = true) MultipartFile arquivo) throws IOException;

    @ApiOperation("Exclui a foto do produto de um restaurante")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Foto do produto excluída"),
		   @ApiResponse(code = SC_BAD_REQUEST, message = "ID do restaurante ou produto inválido", response = Problem.class),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Foto de produto não encontrada", response = Problem.class)})
    ResponseEntity<?> excluir(@ApiParam(value = "ID do restaurante", example = "1", required = true) Long restauranteId,
			      @ApiParam(value = "ID do produto", example = "1", required = true) Long produtoId);

    @ApiOperation(value = "Busca a foto do produto de um restaurante", produces = "application/json, image/jpeg, image/png")
    @ApiResponses({@ApiResponse(code = SC_BAD_REQUEST, message = "ID do restaurante ou produto inválido", response = Problem.class),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Foto de produto não encontrada", response = Problem.class)})
    ResponseEntity<FotoProdutoModel> buscar(@ApiParam(value = "ID do restaurante", example = "1", required = true) Long restauranteId,
					    @ApiParam(value = "ID do produto", example = "1", required = true) Long produtoId);

    @ApiOperation(value = "Busca a foto do produto de um restaurante", hidden = true)
    ResponseEntity<?> servir(Long restauranteId, Long produtoId, String acceptHeader) throws HttpMediaTypeNotAcceptableException;
}
