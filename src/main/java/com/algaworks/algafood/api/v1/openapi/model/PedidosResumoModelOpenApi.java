package com.algaworks.algafood.api.v1.openapi.model;

import com.algaworks.algafood.api.v1.model.PedidoResumoModel;
import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Data;
import org.springframework.hateoas.Links;

@Data
@ApiModel("PedidosResumoModel")
public class PedidosResumoModelOpenApi {

    private PedidosResumoEmbeddedModelApi _embedded;
    private Links                         _links;
    private PageableModelOpenApi          page;

    @Data
    @ApiModel("PedidosResumoEmbeddedModel")
    public class PedidosResumoEmbeddedModelApi {

	private List<PedidoResumoModel> pedidos;
    }

}
