package com.algaworks.algafood.api.v1.openapi.model;

import com.algaworks.algafood.api.v1.model.UsuarioModel;
import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.Links;

@ApiModel("UsuariosModel")
@Getter
@Setter
public class UsuariosModelOpenApi {

    private UsuariosEmbeddedModelApi _embedded;
    private Links                    _links;

    @Getter
    @Setter
    @ApiModel("UsuariosEmbeddedModel")
    public class UsuariosEmbeddedModelApi {

	private List<UsuarioModel> usuarios;
    }
}
