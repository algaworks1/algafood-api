package com.algaworks.algafood.api.v1.openapi.controller;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.v1.model.ProdutoModel;
import com.algaworks.algafood.api.v1.model.input.ProdutoInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;

import static org.apache.http.HttpStatus.SC_CREATED;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;
import static org.apache.http.HttpStatus.SC_OK;

@Api(tags = "Produtos")
public interface RestauranteProdutoControllerOpenApi {

    @ApiOperation("Lista os produtos de um restaurante")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "ID do restaurante inválido", response = Problem.class),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Restaurante não encontrado", response = Problem.class)})
    CollectionModel<ProdutoModel> listar(@ApiParam(value = "ID do restaurante", example = "1", required = true) Long restauranteId,
					 @ApiParam(value = "Indica se deve ou não incluir produtos inativos no resultado da listagem", example = "false", defaultValue = "false") Boolean incluirInativos);

    @ApiOperation("Busca um produto de um restaurante")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "ID do restaurante ou produto inválido", response = Problem.class),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Produto de restaurante não encontrado", response = Problem.class)})
    ResponseEntity<ProdutoModel> buscar(@ApiParam(value = "ID do restaurante", example = "1", required = true) Long restauranteId,

			@ApiParam(value = "ID do produto", example = "1", required = true) Long produtoId);

    @ApiOperation("Cadastra um produto de um restaurante")
    @ApiResponses({@ApiResponse(code = SC_CREATED, message = "Produto cadastrado"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Restaurante não encontrado", response = Problem.class)})
    ResponseEntity<ProdutoModel> adicionar(@ApiParam(value = "ID do restaurante", example = "1", required = true) Long restauranteId,
					   @ApiParam(name = "corpo", value = "Representação de um novo produto", required = true) ProdutoInput produtoInput);

    @ApiOperation("Atualiza um produto de um restaurante")
    @ApiResponses({@ApiResponse(code = SC_OK, message = "Produto atualizado"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Produto de restaurante não encontrado", response = Problem.class)})
    ResponseEntity<ProdutoModel> atualizar(@ApiParam(value = "ID do restaurante", example = "1", required = true) Long restauranteId,
					   @ApiParam(value = "ID do produto", example = "1", required = true) Long produtoId,
					   @ApiParam(name = "corpo", value = "Representação de um produto com os novos dados", required = true) ProdutoInput produtoInput);
}
