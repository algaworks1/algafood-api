package com.algaworks.algafood.api.v1.openapi.model;

import com.algaworks.algafood.api.v1.model.ProdutoModel;
import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.Links;

@ApiModel("ProdutosModel")
@Getter
@Setter
public class ProdutosModelOpenApi {

    private ProdutosEmbeddedModelApi _embedded;
    private Links                    _links;

    @Getter
    @Setter
    @ApiModel("ProdutosEmbeddedModel")
    public class ProdutosEmbeddedModelApi {

	private List<ProdutoModel> produtos;
    }

}
