package com.algaworks.algafood.api.v1.assembler;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.controller.PedidoController;
import com.algaworks.algafood.api.v1.model.PedidoModel;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.domain.model.Pedido;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class PedidoModelAssembler
		extends RepresentationModelAssemblerSupport<Pedido, PedidoModel> {

    private final ModelMapper modelMapper;
    private final AlgaLinks algaLinks;
    private final AlgaSecurity algaSecurity;

    public PedidoModelAssembler(ModelMapper modelMapper, AlgaLinks algaLinks, AlgaSecurity algaSecurity) {
	super(PedidoController.class, PedidoModel.class);
	this.modelMapper = modelMapper;
	this.algaLinks = algaLinks;
	this.algaSecurity = algaSecurity;
    }

    @Override
    public PedidoModel toModel(Pedido pedido) {
	PedidoModel pedidoModel = createModelWithId(pedido.getCodigo(), pedido);
	modelMapper.map(pedido, pedidoModel);

	// Não usei o método algaSecurity.podePesquisarPedidos(clienteId, restauranteId) aqui,
	// porque na geração do link, não temos o id do cliente e do restaurante,
	// então precisamos saber apenas se a requisição está autenticada e tem o escopo de leitura
	if (algaSecurity.podePesquisarPedidos()) {
	    pedidoModel.add(algaLinks.linkToPedidos("pedidos"));
	}

	if (algaSecurity.podeGerenciarPedidos(pedidoModel.getCodigo())) {
	    if (pedido.podeAlterarParaConfirmado()) {
		pedidoModel.add(algaLinks.linkToConfirmacaoPedido(pedido.getCodigo(), "confirmar"));
	    }

	    if (pedido.podeAlterarParaCancelado()) {
		pedidoModel.add(algaLinks.linkToCancelaPedido(pedido.getCodigo(), "cancelar"));
	    }

	    if (pedido.podeAlterarParaEntregue()) {
		pedidoModel.add(algaLinks.linkToEntregaPedido(pedido.getCodigo(), "entregar"));
	    }
	}

	if (algaSecurity.podeConsultarRestaurantes()) {
	    pedidoModel.getRestaurante().add(algaLinks.linkToRestaurante(pedidoModel.getRestaurante().getId()));
	}

	if (algaSecurity.podeConsultarUsuariosGruposPermissoes()) {
	    pedidoModel.getCliente().add(algaLinks.linkToUsuario(pedidoModel.getCliente().getId()));
	}

	if (algaSecurity.podeConsultarFormasPagamento()) {
	    pedidoModel.getFormaPagamento().add(algaLinks.linkToFormasPagamento(pedido.getFormaPagamento().getId()));
	}

	if (algaSecurity.podeConsultarCidades()) {
	    pedidoModel.getEnderecoEntrega().getCidade().add(algaLinks.linkToCidade(pedido.getEnderecoEntrega().getCidade().getId()));
	}

	// Quem pode consultar restaurantes, também pode consultar os produtos dos restaurantes
	if (algaSecurity.podeConsultarRestaurantes()) {
	    pedidoModel.getItens().forEach(item -> item.add(algaLinks.linkToProduto(pedido.getRestaurante().getId(), item.getProdutoId(), "produto")));
	}

	return pedidoModel;
    }
}
