package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.assembler.UsuarioModelAssembler;
import com.algaworks.algafood.api.v1.model.UsuarioModel;
import com.algaworks.algafood.api.v1.openapi.controller.RestauranteUsuarioResponsavelControllerOpenApi;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.service.CadastroRestauranteService;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(path = "/v1/restaurantes/{restauranteId}/responsaveis", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestauranteUsuarioResponsavelController implements RestauranteUsuarioResponsavelControllerOpenApi {

    private final CadastroRestauranteService cadastroRestauranteService;
    private final UsuarioModelAssembler usuarioModelAssembler;
    private final AlgaLinks algaLinks;
    private final AlgaSecurity algaSecurity;

    @CheckSecurity.Restaurantes.PodeGerenciarCadastro
    @GetMapping
    @Override
    public CollectionModel<UsuarioModel> listar(@PathVariable Long restauranteId) {
	Restaurante restaurante = cadastroRestauranteService.buscaPorId(restauranteId);

	CollectionModel<UsuarioModel> usuariosModel = usuarioModelAssembler
			.toCollectionModel(restaurante.getResponsaveis())
			.removeLinks();

	usuariosModel.add(algaLinks.linkToRestauranteResponsaveis(restauranteId));

	if (algaSecurity.podeGerenciarCadastroRestaurantes()) {
	    usuariosModel.add(algaLinks.linkToRestauranteResponsavelAssociacao(restauranteId, "associar"));

	    usuariosModel.getContent().forEach(usuarioModel -> {
		usuarioModel.add(algaLinks.linkToRestauranteResponsavelDesassociacao(restauranteId, usuarioModel.getId(), "desassociar"));
	    });
	}

	return usuariosModel;
    }

    @CheckSecurity.Restaurantes.PodeGerenciarCadastro
    @PutMapping("/{usuarioId}")
    @Override
    public ResponseEntity<?> associar(@PathVariable Long restauranteId, @PathVariable Long usuarioId) {
	try {
	    cadastroRestauranteService.associarPermissao(restauranteId, usuarioId);
	    return ResponseEntity.noContent().build();
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

    @CheckSecurity.Restaurantes.PodeGerenciarCadastro
    @DeleteMapping("/{usuarioId}")
    @Override
    public ResponseEntity<?> desassociar(@PathVariable Long restauranteId, @PathVariable Long usuarioId) {
	try {
	    cadastroRestauranteService.desassociarPermissao(restauranteId, usuarioId);
	    return ResponseEntity.noContent().build();
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

}
