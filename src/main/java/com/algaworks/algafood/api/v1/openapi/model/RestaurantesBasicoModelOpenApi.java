package com.algaworks.algafood.api.v1.openapi.model;

import com.algaworks.algafood.api.v1.model.RestauranteBasicoModel;
import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.Links;

@ApiModel("RestaurantesBasicoModel")
@Getter
@Setter
public class RestaurantesBasicoModelOpenApi {

    private RestaurantesBasicoEmbeddedModelApi _embedded;
    private Links                              _links;

    @Getter
    @Setter
    @ApiModel("RetaurantesBasicoEmbeddedModel")
    public class RestaurantesBasicoEmbeddedModelApi {

	private List<RestauranteBasicoModel> restaurantes;
    }
}

