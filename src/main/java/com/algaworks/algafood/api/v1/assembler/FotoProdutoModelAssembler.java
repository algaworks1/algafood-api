package com.algaworks.algafood.api.v1.assembler;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.controller.RestauranteProdutoFotoController;
import com.algaworks.algafood.api.v1.model.FotoProdutoModel;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.domain.model.FotoProduto;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class FotoProdutoModelAssembler extends RepresentationModelAssemblerSupport<FotoProduto, FotoProdutoModel> {

    private final ModelMapper modelMapper;
    private final AlgaLinks algaLinks;
    private final AlgaSecurity algaSecurity;

    public FotoProdutoModelAssembler(ModelMapper modelMapper, AlgaLinks algaLinks, AlgaSecurity algaSecurity) {
        super(RestauranteProdutoFotoController.class, FotoProdutoModel.class);
	this.modelMapper = modelMapper;
	this.algaLinks = algaLinks;
	this.algaSecurity = algaSecurity;
    }

    @Override
    public FotoProdutoModel toModel(FotoProduto fotoProduto) {
	FotoProdutoModel fotoProdutoModel = modelMapper.map(fotoProduto, FotoProdutoModel.class);

	// Quem pode consultar restaurantes, também pode consultar os produtos e fotos
	if (algaSecurity.podeConsultarRestaurantes()) {
	    fotoProdutoModel.add(algaLinks.linkToProdutoFoto(fotoProduto.getRestauranteId(), fotoProduto.getProduto().getId(), "foto"));
	    fotoProdutoModel.add(algaLinks.linkToProduto(fotoProduto.getRestauranteId(), fotoProduto.getProduto().getId(), "produto"));
	}

	return fotoProdutoModel;
    }
}
