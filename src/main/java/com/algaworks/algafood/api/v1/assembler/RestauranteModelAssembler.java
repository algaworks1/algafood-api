package com.algaworks.algafood.api.v1.assembler;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.controller.RestauranteController;
import com.algaworks.algafood.api.v1.model.EnderecoModel;
import com.algaworks.algafood.api.v1.model.RestauranteModel;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.domain.model.Restaurante;
import java.util.Optional;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class RestauranteModelAssembler extends RepresentationModelAssemblerSupport<Restaurante, RestauranteModel> {

    private final ModelMapper modelMapper;
    private final AlgaLinks    algaLinks;
    private final AlgaSecurity algaSecurity;

    public RestauranteModelAssembler(ModelMapper modelMapper, AlgaLinks algaLinks, AlgaSecurity algaSecurity) {
	super(RestauranteController.class, RestauranteModel.class);
	this.modelMapper = modelMapper;
	this.algaLinks = algaLinks;
	this.algaSecurity = algaSecurity;
    }

    public RestauranteModel toModel(Restaurante restaurante) {
	RestauranteModel restauranteModel = createModelWithId(restaurante.getId(), restaurante);
	modelMapper.map(restaurante, restauranteModel);

	if (algaSecurity.podeConsultarRestaurantes()) {
	    restauranteModel.add(algaLinks.linkToRestaurantes("restaurantes"));
	}

	if (algaSecurity.podeGerenciarCadastroRestaurantes()) {
	    if (restaurante.ativacaoPermitida()) {
		restauranteModel.add(algaLinks.linkToRestauranteAtivacao(restauranteModel.getId(), "ativar"));
	    }

	    if (restaurante.inativacaoPermitida()) {
		restauranteModel.add(algaLinks.linkToRestauranteInativacao(restauranteModel.getId(), "inativar"));
	    }
	}

	if (algaSecurity.podeGerenciarFuncionamentoRestaurantes(restaurante.getId())) {
	    if (restaurante.aberturaPermitida()) {
		restauranteModel.add(algaLinks.linkToRestauranteAbertura(restauranteModel.getId(), "abrir"));
	    }

	    if (restaurante.fechamentoPermitido()) {
		restauranteModel.add(algaLinks.linkToRestauranteFechamento(restauranteModel.getId(), "fechar"));
	    }
	}

	if (algaSecurity.podeConsultarRestaurantes()) {
	    restauranteModel.add(algaLinks.linkToProdutos(restaurante.getId(), "produtos"));
	}

	if (algaSecurity.podeConsultarCozinhas()) {
	    restauranteModel.getCozinha().add(algaLinks.linkToCozinha(restauranteModel.getCozinha().getId()));
	}

	if (algaSecurity.podeConsultarCidades()) {
	    Optional.ofNullable(restauranteModel.getEndereco())
			    .map(EnderecoModel::getCidade)
			    .ifPresent(cidade -> cidade.add(algaLinks.linkToCidade(cidade.getId())));
	}

	if (algaSecurity.podeConsultarRestaurantes()) {
	    restauranteModel.add(algaLinks.linkToRestauranteFormasPagamento(restauranteModel.getId(), "formas-pagamento"));
	}

	if (algaSecurity.podeGerenciarCadastroRestaurantes()) {
	    restauranteModel.add(algaLinks.linkToRestauranteResponsaveis(restauranteModel.getId(), "responsaveis"));
	}

	return restauranteModel;
    }

    @Override
    public CollectionModel<RestauranteModel> toCollectionModel(Iterable<? extends Restaurante> restaurantes) {
	CollectionModel<RestauranteModel> collectionModel = super.toCollectionModel(restaurantes);

	if (algaSecurity.podeConsultarRestaurantes()) {
	    collectionModel.add(algaLinks.linkToRestaurantes());
	}

	return collectionModel;
    }
}
