package com.algaworks.algafood.api.v1.openapi.controller;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.v1.model.GrupoModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;

import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;

@Api(tags = "Usuários")
public interface UsuarioGrupoControllerOpenApi {

    @ApiOperation("Lista os grupos associados a um usuário")
    @ApiResponses({@ApiResponse(code = SC_NOT_FOUND, message = "Usuário não encontrado", response = Problem.class)})
    CollectionModel<GrupoModel> listar(@ApiParam(value = "ID do usuário", example = "1", required = true) Long usuarioId);

    @ApiOperation("Desassociação de grupo com usuário")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Desassociação realizada com sucesso"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Usuário ou grupo não encontrado", response = Problem.class)})
    ResponseEntity<?> desassociar(@ApiParam(value = "ID do usuário", example = "1", required = true) Long usuarioId,

				  @ApiParam(value = "ID do grupo", example = "1", required = true) Long grupoId);

    @ApiOperation("Associação de grupo com usuário")
    @ApiResponses({@ApiResponse(code = SC_NO_CONTENT, message = "Associação realizada com sucesso"),
		   @ApiResponse(code = SC_NOT_FOUND, message = "Usuário ou grupo não encontrado", response = Problem.class)})
    ResponseEntity<?> associar(@ApiParam(value = "ID do usuário", example = "1", required = true) Long usuarioId,
			       @ApiParam(value = "ID do grupo", example = "1", required = true) Long grupoId);
}
