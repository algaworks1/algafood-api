package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.model.EstatisticasModel;
import com.algaworks.algafood.api.v1.model.filter.VendaDiariaFilter;
import com.algaworks.algafood.api.v1.openapi.controller.EstatisticaControllerOpenApi;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.model.dto.VendaDiaria;
import com.algaworks.algafood.domain.service.VendaDiariaQueryService;
import com.algaworks.algafood.domain.service.VendaDiariaReportService;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RequestMapping(path = "/v1/estatisticas", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class EstatisticasController
		implements EstatisticaControllerOpenApi {

    private final VendaDiariaQueryService vendaDiariaQueryService;
    private final VendaDiariaReportService vendaDiariaReportService;
    private final AlgaLinks algaLinks;

    @CheckSecurity.Estatisticas.PodeConsultar
    @GetMapping
    @Override
    public EstatisticasModel estatisticas() {
	EstatisticasModel estatisticasModel = new EstatisticasModel();
	estatisticasModel.add(algaLinks.linkToEstatisticasVendasDiarias("vendas-diarias"));
	return estatisticasModel;
    }

    @CheckSecurity.Estatisticas.PodeConsultar
    @GetMapping(path = "/vendas-diarias")
    @Override
    public ResponseEntity<List<VendaDiaria>> consultarVendasDiarias(VendaDiariaFilter filter,
								    @RequestParam(required = false, defaultValue = "+00:00") String timeOffSet) {
	List<VendaDiaria> vendaDiarias = vendaDiariaQueryService.consultarVendasDiarias(filter, timeOffSet);
	return ResponseEntity.ok(vendaDiarias);
    }

    @CheckSecurity.Estatisticas.PodeConsultar
    @GetMapping(path = "/vendas-diarias", produces = MediaType.APPLICATION_PDF_VALUE)
    @Override
    public ResponseEntity<byte[]> consultarVendasDiariasPdf(VendaDiariaFilter filter,
						    @RequestParam(required = false, defaultValue = "+00:00") String timeOffSet) {

        byte[] bytesPdf = vendaDiariaReportService.consultarVendasDiarias(filter, timeOffSet);

	HttpHeaders headers = new HttpHeaders();
	headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=vendas-diarias.pdf");

	return ResponseEntity.ok()
			.contentType(MediaType.APPLICATION_PDF)
			.headers(headers)
			.body(bytesPdf);
    }

}
