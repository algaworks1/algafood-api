package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.assembler.InputDisassembler;
import com.algaworks.algafood.api.v1.assembler.ProdutoModelAssembler;
import com.algaworks.algafood.api.v1.model.ProdutoModel;
import com.algaworks.algafood.api.v1.model.input.ProdutoInput;
import com.algaworks.algafood.api.v1.openapi.controller.RestauranteProdutoControllerOpenApi;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.model.Produto;
import com.algaworks.algafood.domain.service.CadastroProdutoService;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RequestMapping(path = "/v1/restaurantes/{restauranteId}/produtos", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class RestauranteProdutoController implements RestauranteProdutoControllerOpenApi {

    private final CadastroProdutoService                   cadastroProdutoService;
    private final ProdutoModelAssembler                    modelAssembler;
    private final InputDisassembler<ProdutoInput, Produto> inputDisassembler;
    private final AlgaLinks                                algaLinks;

    @CheckSecurity.Restaurantes.PodeConsultar
    @GetMapping
    @Override
    public CollectionModel<ProdutoModel> listar(@PathVariable Long restauranteId,
						@RequestParam(required = false, defaultValue = "false") Boolean incluirInativo) {
	List<Produto> produtos;

	if (incluirInativo) {
	    produtos = cadastroProdutoService.buscaTodosPorRestaurante(restauranteId);
	} else {
	    produtos = cadastroProdutoService.buscaAtivosPorRestaurante(restauranteId);
	}

	return modelAssembler.toCollectionModel(produtos)
			.add(algaLinks.linkToProdutos(restauranteId));
    }

    @CheckSecurity.Restaurantes.PodeConsultar
    @GetMapping("/{produtoId}")
    @Override
    public ResponseEntity<ProdutoModel> buscar(@PathVariable Long restauranteId, @PathVariable Long produtoId) {
	Produto produto = cadastroProdutoService.buscaPorId(restauranteId, produtoId);
	return ResponseEntity.ok(modelAssembler.toModel(produto));
    }

    @CheckSecurity.Restaurantes.PodeGerenciarFuncionamento
    @PostMapping
    @Override
    public ResponseEntity<ProdutoModel> adicionar(@PathVariable Long restauranteId, @RequestBody @Valid ProdutoInput produtoInput) {
	Produto produto = inputDisassembler.toDomainObject(produtoInput, Produto.class);
	cadastroProdutoService.salvar(restauranteId, produto);
	return ResponseEntity.ok(modelAssembler.toModel(produto));
    }

    @CheckSecurity.Restaurantes.PodeGerenciarFuncionamento
    @PutMapping("/{produtoId}")
    @Override
    public ResponseEntity<ProdutoModel> atualizar(@PathVariable Long restauranteId, @PathVariable Long produtoId,
						  @RequestBody @Valid ProdutoInput produtoInput) {
	Produto produto = cadastroProdutoService.buscaPorId(restauranteId, produtoId);
	inputDisassembler.copyToDomainObject(produtoInput, produto);
	cadastroProdutoService.salvar(restauranteId, produto);
	return ResponseEntity.ok(modelAssembler.toModel(produto));
    }

}
