package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.ResourceUriHelper;
import com.algaworks.algafood.api.v1.assembler.CidadeInputDisassembler;
import com.algaworks.algafood.api.v1.assembler.CidadeModelAssembler;
import com.algaworks.algafood.api.v1.model.CidadeModel;
import com.algaworks.algafood.api.v1.model.input.CidadeInput;
import com.algaworks.algafood.api.v1.openapi.controller.CidadeControllerOpenApi;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.repository.CidadeRepository;
import com.algaworks.algafood.domain.service.CadastroCidadeService;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Data
@AllArgsConstructor
@RestController
@RequestMapping(path = "/v1/cidades", produces = MediaType.APPLICATION_JSON_VALUE)
public class CidadeController implements CidadeControllerOpenApi {

    private final CidadeRepository                    cidadeRepository;
    private final CadastroCidadeService   cadastroCidadeService;
    private final CidadeModelAssembler    modelAssembler;
    private final CidadeInputDisassembler inputDisassembler;

    @CheckSecurity.Cidades.PodeConsultar
    @GetMapping
    @Override
    public CollectionModel<CidadeModel> listar() {
	List<Cidade> cidades = cidadeRepository.findAll();
	return modelAssembler.toCollectionModel(cidades);
    }

    @CheckSecurity.Cidades.PodeConsultar
    @GetMapping("/{cidadeId}")
    @Override
    public CidadeModel buscar(@PathVariable Long cidadeId) {
	Cidade cidade = cadastroCidadeService.buscaPorId(cidadeId);
	return modelAssembler.toModel(cidade);
    }

    @CheckSecurity.Cidades.PodeEditar
    @PostMapping
    @Override
    public CidadeModel adicionar(@RequestBody @Valid CidadeInput cidadeInput) {
	try {
	    Cidade cidade = inputDisassembler.toDomainObject(cidadeInput, Cidade.class);
	    cadastroCidadeService.salvar(cidade);
	    CidadeModel cidadeModel = modelAssembler.toModel(cidade);
	    ResourceUriHelper.addUriInResponseHeader(cidadeModel.getId());
	    return cidadeModel;
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

    @CheckSecurity.Cidades.PodeEditar
    @PutMapping("/{cidadeId}")
    @Override
    public ResponseEntity<CidadeModel> atualizar(@PathVariable Long cidadeId, @RequestBody @Valid CidadeInput cidadeInput) {
	try {
	    Cidade cidade = this.cadastroCidadeService.buscaPorId(cidadeId);
	    inputDisassembler.copyToDomainObject(cidadeInput, cidade);
	    this.cadastroCidadeService.salvar(cidade);
	    return ResponseEntity.ok(modelAssembler.toModel(cidade));
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}

    }

    @CheckSecurity.Cidades.PodeEditar
    @DeleteMapping("/{cidadeId}")
    @Override
    public ResponseEntity<?> remover(@PathVariable Long cidadeId) {
	this.cadastroCidadeService.excluir(cidadeId);
	return ResponseEntity.noContent().build();
    }

}

