package com.algaworks.algafood.api.v1.assembler;

import com.algaworks.algafood.api.v1.model.input.RestauranteInput;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.model.Restaurante;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class RestauranteInputDisassembler extends InputDisassembler<RestauranteInput, Restaurante> {

    public RestauranteInputDisassembler(ModelMapper modelMapper) {
	super(modelMapper);
    }

    @Override
    public void copyToDomainObject(RestauranteInput restauranteInput, Restaurante restauranteDestination) {
	// Para corrigir org.springframework.orm.jpa.JpaSystemException: identifier of an instance
	// of com.algaworks.algafood.domain.model.Cozinha was altered from 1 to 2
	restauranteDestination.setCozinha(new Cozinha());
	super.copyToDomainObject(restauranteInput, restauranteDestination);
    }
}
