package com.algaworks.algafood.api.v1.openapi.model;

import com.algaworks.algafood.api.v1.model.CozinhaModel;
import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.Links;

@ApiModel("CozinhasModel")
@Getter
@Setter
public class CozinhasModelOpenApi {

    private CozinhasEmbeddedModelApi _embedded;
    private Links                    _links;
    private PageModelOpenApi         page;

    @Getter
    @Setter
    @ApiModel("CozinhasEmbeddedModel")
    public class CozinhasEmbeddedModelApi {

	private List<CozinhaModel> cozinhas;
    }
}
