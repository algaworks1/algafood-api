package com.algaworks.algafood.api.v1.assembler;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class ModelAssembler<I, O> {

    private final ModelMapper modelMapper;

    public O toModel(I inputObject, Class<O> outputTypeClass) {
	return modelMapper.map(inputObject, outputTypeClass);
    }

    public List<O> toCollectionModel(Collection<I> inputObjectList, Class<O> outputTypeClass) {
	return inputObjectList.stream().map((I inputObject) -> toModel(inputObject, outputTypeClass)).collect(Collectors.toList());
    }
}
