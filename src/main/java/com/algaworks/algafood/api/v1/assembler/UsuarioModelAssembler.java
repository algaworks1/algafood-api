package com.algaworks.algafood.api.v1.assembler;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.controller.UsuarioController;
import com.algaworks.algafood.api.v1.model.UsuarioModel;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.domain.model.Usuario;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class UsuarioModelAssembler
		extends RepresentationModelAssemblerSupport<Usuario, UsuarioModel> {

    private final ModelMapper modelMapper;
    private final AlgaLinks    algaLinks;
    private final AlgaSecurity algaSecurity;

    public UsuarioModelAssembler(ModelMapper modelMapper, AlgaLinks algaLinks, AlgaSecurity algaSecurity) {
	super(UsuarioController.class, UsuarioModel.class);
	this.modelMapper = modelMapper;
	this.algaLinks = algaLinks;
	this.algaSecurity = algaSecurity;
    }

    @Override
    public UsuarioModel toModel(Usuario usuario) {
	UsuarioModel usuarioModel = createModelWithId(usuario.getId(), usuario);
	modelMapper.map(usuario, usuarioModel);

	if (algaSecurity.podeConsultarUsuariosGruposPermissoes()) {
	    usuarioModel.add(algaLinks.linkToUsuarios("usuarios"));
	    usuarioModel.add(algaLinks.linkToGruposUsuario(usuario.getId(), "grupos-usuario"));
	}

	return usuarioModel;
    }

    @Override
    public CollectionModel<UsuarioModel> toCollectionModel(Iterable<? extends Usuario> usuarios) {
	return super.toCollectionModel(usuarios)
			.add(algaLinks.linkToUsuarios());
    }
}
