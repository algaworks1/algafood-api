package com.algaworks.algafood.api.v1.openapi.controller;

import com.algaworks.algafood.api.v1.model.EstatisticasModel;
import com.algaworks.algafood.api.v1.model.filter.VendaDiariaFilter;
import com.algaworks.algafood.domain.model.dto.VendaDiaria;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import org.springframework.http.ResponseEntity;

@Api(tags = "Estatisticas")
public interface EstatisticaControllerOpenApi {

    @ApiOperation(value = "Estatísticas", hidden = true)
    EstatisticasModel estatisticas();

    @ApiImplicitParams({
		    @ApiImplicitParam(value = "Data/hora inicial da criação do pedido", name = "dataCriacaoInicio", paramType = "query", type = "date-time"),
		    @ApiImplicitParam(value = "Data/hora final da criação do pedido", name = "dataCriacaoFim", paramType = "query", type = "date-time"),
		    @ApiImplicitParam(value = "ID do restaurante", name = "restauranteId", paramType = "query", type = "int")
    })
    @ApiOperation("Consulta estatísticas de vendas diárias")
    ResponseEntity<List<VendaDiaria>> consultarVendasDiarias(VendaDiariaFilter filter,
							     @ApiParam(name = "timeOffset", value = "Deslocamento do horário a ser considerado na consulta em relação ao UTC", defaultValue = "+00:00") String timeOffSet);

    ResponseEntity<byte[]> consultarVendasDiariasPdf(VendaDiariaFilter filter, String timeOffSet);
}
