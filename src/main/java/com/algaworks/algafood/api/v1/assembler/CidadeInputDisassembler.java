package com.algaworks.algafood.api.v1.assembler;

import com.algaworks.algafood.api.v1.model.input.CidadeInput;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Estado;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CidadeInputDisassembler
		extends InputDisassembler<CidadeInput, Cidade> {

    public CidadeInputDisassembler(ModelMapper modelMapper) {
	super(modelMapper);
    }

    @Override
    public void copyToDomainObject(CidadeInput cidadeInput, Cidade cidadeDestination) {
	// Para corrigir org.springframework.orm.jpa.JpaSystemException: identifier of an instance
	// of com.algaworks.algafood.domain.model.Cozinha was altered from 1 to 2
	cidadeDestination.setEstado(new Estado());
	super.copyToDomainObject(cidadeInput, cidadeDestination);
    }
}
