package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.v1.assembler.InputDisassembler;
import com.algaworks.algafood.api.v1.assembler.UsuarioModelAssembler;
import com.algaworks.algafood.api.v1.model.UsuarioModel;
import com.algaworks.algafood.api.v1.model.input.SenhaInput;
import com.algaworks.algafood.api.v1.model.input.UsuarioComSenhaInput;
import com.algaworks.algafood.api.v1.model.input.UsuarioInput;
import com.algaworks.algafood.api.v1.openapi.controller.UsuarioControllerOpenApi;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.exception.SenhaInvalidaException;
import com.algaworks.algafood.domain.model.Usuario;
import com.algaworks.algafood.domain.repository.UsuarioRepository;
import com.algaworks.algafood.domain.service.CadastroUsuarioService;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(path = "/v1/usuarios", produces = MediaType.APPLICATION_JSON_VALUE)
public class UsuarioController implements UsuarioControllerOpenApi {

    private final UsuarioRepository                                usuarioRepository;
    private final CadastroUsuarioService                   cadastroUsuarioService;
    private final UsuarioModelAssembler                    modelAssembler;
    private final InputDisassembler<UsuarioInput, Usuario> usuarioInputDisassembler;
    private final InputDisassembler<UsuarioComSenhaInput, Usuario> usuarioComSenhaInputDisassembler;

    @CheckSecurity.UsuariosGruposPermissoes.PodeConsultar
    @GetMapping
    @Override
    public CollectionModel<UsuarioModel> listar() {
	List<Usuario> usuarios = usuarioRepository.findAll();
	return modelAssembler.toCollectionModel(usuarios);
    }

    @CheckSecurity.UsuariosGruposPermissoes.PodeConsultar
    @GetMapping("/{usuarioId}")
    @Override
    public UsuarioModel buscar(@PathVariable Long usuarioId) {
	Usuario usuario = this.cadastroUsuarioService.buscaPorId(usuarioId);
	return modelAssembler.toModel(usuario);
    }

    @CheckSecurity.UsuariosGruposPermissoes.PodeEditar
    @PostMapping
    @Override
    public UsuarioModel adicionar(@RequestBody @Valid UsuarioComSenhaInput usuarioComSenhaInput) {
	Usuario usuario = usuarioComSenhaInputDisassembler.toDomainObject(usuarioComSenhaInput, Usuario.class);
	cadastroUsuarioService.salvar(usuario);
	return modelAssembler.toModel(usuario);
    }

    @CheckSecurity.UsuariosGruposPermissoes.PodeAlterarProprioUsuario
    @PutMapping("/{usuarioId}")
    @Override
    public UsuarioModel atualizar(@PathVariable Long usuarioId, @RequestBody @Valid UsuarioInput usuarioInput) {
	try {
	    Usuario usuario = cadastroUsuarioService.buscaPorId(usuarioId);
	    usuarioInputDisassembler.copyToDomainObject(usuarioInput, usuario);
	    cadastroUsuarioService.salvar(usuario);
	    return modelAssembler.toModel(usuario);
	} catch (RecursoNaoEncontradoException | SenhaInvalidaException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

    @CheckSecurity.UsuariosGruposPermissoes.PodeAlterarPropriaSenha
    @PutMapping("/{usuarioId}/senha")
    @Override
    public ResponseEntity<?> alterarSenha(@PathVariable Long usuarioId, @RequestBody @Valid SenhaInput senhaInput) {
	try {
	    cadastroUsuarioService.alterarSenha(usuarioId, senhaInput.getSenhaAtual(), senhaInput.getNovaSenha());
	    return ResponseEntity.noContent().build();
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

}
