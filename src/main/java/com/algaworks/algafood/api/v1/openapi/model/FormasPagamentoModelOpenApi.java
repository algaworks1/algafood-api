package com.algaworks.algafood.api.v1.openapi.model;

import com.algaworks.algafood.api.v1.model.FormaPagamentoModel;
import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Data;
import org.springframework.hateoas.Links;

@Data
@ApiModel("FormasPagamentoModel")
public class FormasPagamentoModelOpenApi {

    private FormasPagamentoEmbeddedModelOpenApi _embedded;
    private Links                               _links;

    @Data
    @ApiModel("FormasPagamentoEmbeddedModel")
    public class FormasPagamentoEmbeddedModelOpenApi {

	private List<FormaPagamentoModel> formasPagamento;
    }
}
