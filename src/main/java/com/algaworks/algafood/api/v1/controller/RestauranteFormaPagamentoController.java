package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.v1.AlgaLinks;
import com.algaworks.algafood.api.v1.assembler.FormaPagamentoModelAssembler;
import com.algaworks.algafood.api.v1.model.FormaPagamentoModel;
import com.algaworks.algafood.api.v1.openapi.controller.RestauranteFormaPagamentoControllerOpenApi;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.service.CadastroRestauranteService;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RequestMapping(path = "/v1/restaurantes/{restauranteId}/formas-pagamento", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class RestauranteFormaPagamentoController implements RestauranteFormaPagamentoControllerOpenApi {

    private final CadastroRestauranteService   cadastroRestauranteService;
    private final FormaPagamentoModelAssembler formaPagamentoModelAssembler;
    private final AlgaLinks    algaLinks;
    private final AlgaSecurity algaSecurity;

    @CheckSecurity.Restaurantes.PodeConsultar
    @GetMapping
    @Override
    public CollectionModel<FormaPagamentoModel> listar(@PathVariable Long restauranteId) {
	Restaurante restaurante = cadastroRestauranteService.buscaPorId(restauranteId);

	CollectionModel<FormaPagamentoModel> formasPagamentoModel = formaPagamentoModelAssembler
									.toCollectionModel(restaurante.getFormasPagamento())
									.removeLinks();

	formasPagamentoModel.add(algaLinks.linkToRestauranteFormasPagamento(restauranteId));

	if (algaSecurity.podeGerenciarFuncionamentoRestaurantes(restauranteId)) {
	    formasPagamentoModel.add(algaLinks.linkToRestauranteFormaPagamentoAssociacao(restauranteId, "associar"));

	    formasPagamentoModel.getContent().forEach(formaPagamentoModel -> {
		formaPagamentoModel.add(algaLinks.linkToRestauranteFormaPagamentoDesassociacao(restauranteId, formaPagamentoModel.getId(), "desassociar"));
	    });
	}

	return formasPagamentoModel;
    }

    @CheckSecurity.Restaurantes.PodeGerenciarFuncionamento
    @PutMapping("/{formaPagamentoId}")
    @Override
    public ResponseEntity<?> associar(@PathVariable Long restauranteId, @PathVariable Long formaPagamentoId) {
	try {
	    cadastroRestauranteService.associarFormaPagamento(restauranteId, formaPagamentoId);
	    return ResponseEntity.noContent().build();
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

    @CheckSecurity.Restaurantes.PodeGerenciarFuncionamento
    @DeleteMapping("/{formaPagamentoId}")
    @Override
    public ResponseEntity<?> desassociar(@PathVariable Long restauranteId, @PathVariable Long formaPagamentoId) {
	try {
	    cadastroRestauranteService.desassociarFormaPagamento(restauranteId, formaPagamentoId);
	    return ResponseEntity.noContent().build();
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

}
