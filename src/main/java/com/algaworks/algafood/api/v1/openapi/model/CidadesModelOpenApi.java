package com.algaworks.algafood.api.v1.openapi.model;

import com.algaworks.algafood.api.v1.model.CidadeModel;
import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Data;
import org.springframework.hateoas.Links;

@Data
@ApiModel("CidadesModel")
public class CidadesModelOpenApi {

    private CidadesEmbeddedModelApi _embedded;
    private Links                   _links;

    @Data
    @ApiModel("CidadesEmbeddedModel")
    public class CidadesEmbeddedModelApi {
	private List<CidadeModel> cidades;
    }
}
