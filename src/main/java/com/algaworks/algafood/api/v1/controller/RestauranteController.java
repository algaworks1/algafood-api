package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.ResourceUriHelper;
import com.algaworks.algafood.api.v1.assembler.RestauranteApenasNomeModelAssembler;
import com.algaworks.algafood.api.v1.assembler.RestauranteBasicoModelAssembler;
import com.algaworks.algafood.api.v1.assembler.RestauranteInputDisassembler;
import com.algaworks.algafood.api.v1.assembler.RestauranteModelAssembler;
import com.algaworks.algafood.api.v1.model.RestauranteApenasNomeModel;
import com.algaworks.algafood.api.v1.model.RestauranteBasicoModel;
import com.algaworks.algafood.api.v1.model.RestauranteModel;
import com.algaworks.algafood.api.v1.model.input.RestauranteInput;
import com.algaworks.algafood.api.v1.openapi.controller.RestauranteControllerOpenApi;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.exception.RestauranteNaoEncontradoException;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.RestauranteRepository;
import com.algaworks.algafood.domain.service.CadastroRestauranteService;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Data
@AllArgsConstructor
@RestController
@RequestMapping(path = "/v1/restaurantes", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestauranteController implements RestauranteControllerOpenApi {

    private final RestauranteRepository restauranteRepository;
    private final CadastroRestauranteService   cadastroRestauranteService;
    private final RestauranteModelAssembler       restauranteModelAssembler;
    private final RestauranteBasicoModelAssembler restauranteBasicoModelAssembler;
    private final RestauranteApenasNomeModelAssembler restauranteApenasNomeModelAssembler;
    private final RestauranteInputDisassembler    inputDisassembler;

    @CheckSecurity.Restaurantes.PodeConsultar
    @GetMapping
    @Override
    public CollectionModel<RestauranteBasicoModel> listar() {
	List<Restaurante> restaurantes = restauranteRepository.findAll();
	return restauranteBasicoModelAssembler.toCollectionModel(restaurantes);
    }

    @CheckSecurity.Restaurantes.PodeConsultar
    @GetMapping(params = "projecao=apenas-nome")
    @Override
    public CollectionModel<RestauranteApenasNomeModel> listarApenasNomes() {
	List<Restaurante> restaurantes = restauranteRepository.findAll();
	return restauranteApenasNomeModelAssembler.toCollectionModel(restaurantes);
    }

    @CheckSecurity.Restaurantes.PodeConsultar
    @GetMapping("/{restauranteId}")
    @Override
    public RestauranteModel buscar(@PathVariable Long restauranteId) {
	Restaurante restaurante = this.cadastroRestauranteService.buscaPorId(restauranteId);
	return restauranteModelAssembler.toModel(restaurante);
    }

    @CheckSecurity.Restaurantes.PodeGerenciarCadastro
    @PostMapping
    @Override
    public RestauranteModel adicionar(@RequestBody @Valid RestauranteInput restauranteInput) {
	try {
	    Restaurante restaurante = inputDisassembler.toDomainObject(restauranteInput, Restaurante.class);
	    RestauranteModel restauranteModel = restauranteModelAssembler.toModel(cadastroRestauranteService.salvar(restaurante));
	    ResourceUriHelper.addUriInResponseHeader(restauranteModel.getId());
	    return restauranteModel;
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

    @CheckSecurity.Restaurantes.PodeGerenciarCadastro
    @PutMapping("/{restauranteId}")
    @Override
    public RestauranteModel atualizar(@PathVariable Long restauranteId,
				      @RequestBody @Valid RestauranteInput restauranteInput) {
	try {
	    Restaurante restaurante = cadastroRestauranteService.buscaPorId(restauranteId);
	    inputDisassembler.copyToDomainObject(restauranteInput, restaurante);
	    return restauranteModelAssembler.toModel(cadastroRestauranteService.salvar(restaurante));
	} catch (RecursoNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage());
	}
    }

    @CheckSecurity.Restaurantes.PodeGerenciarCadastro
    @PutMapping("/{restauranteId}/ativo")
    @Override
    public ResponseEntity<?> ativar(@PathVariable Long restauranteId) {
	cadastroRestauranteService.ativar(restauranteId);
	return ResponseEntity.noContent().build();
    }

    @CheckSecurity.Restaurantes.PodeGerenciarCadastro
    @DeleteMapping("/{restauranteId}/ativo")
    @Override
    public ResponseEntity<?> inativar(@PathVariable Long restauranteId) {
	cadastroRestauranteService.inativar(restauranteId);
	return ResponseEntity.noContent().build();
    }

    @CheckSecurity.Restaurantes.PodeGerenciarCadastro
    @PutMapping("/ativacao")
    @Override
    public ResponseEntity<?> ativarMultiplos(@RequestBody List<Long> restauranteIds) {
	try {
	    cadastroRestauranteService.ativar(restauranteIds);
	    return ResponseEntity.noContent().build();
	} catch (RestauranteNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

    @CheckSecurity.Restaurantes.PodeGerenciarCadastro
    @DeleteMapping("/ativacao")
    @Override
    public ResponseEntity<?> inativarMultiplos(@RequestBody List<Long> restauranteIds) {
	try {
	    cadastroRestauranteService.inativar(restauranteIds);
	    return ResponseEntity.noContent().build();
	} catch (RestauranteNaoEncontradoException e) {
	    throw new NegocioException(e.getMessage(), e);
	}
    }

    @CheckSecurity.Restaurantes.PodeGerenciarFuncionamento
    @PutMapping("/{restauranteId}/abertura")
    @Override
    public ResponseEntity<?> abrir(@PathVariable Long restauranteId) {
	cadastroRestauranteService.abrir(restauranteId);
	return ResponseEntity.noContent().build();
    }

    @CheckSecurity.Restaurantes.PodeGerenciarFuncionamento
    @PutMapping("/{restauranteId}/fechamento")
    @Override
    public ResponseEntity<?> fechar(@PathVariable Long restauranteId) {
	cadastroRestauranteService.fechar(restauranteId);
	return ResponseEntity.noContent().build();
    }

}
