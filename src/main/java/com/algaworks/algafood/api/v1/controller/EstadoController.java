package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.ResourceUriHelper;
import com.algaworks.algafood.api.v1.assembler.EstadoModelAssembler;
import com.algaworks.algafood.api.v1.assembler.InputDisassembler;
import com.algaworks.algafood.api.v1.model.EstadoModel;
import com.algaworks.algafood.api.v1.model.input.EstadoInput;
import com.algaworks.algafood.api.v1.openapi.controller.EstadoControllerOpenApi;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RecursoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.repository.EstadoRepository;
import com.algaworks.algafood.domain.service.CadastroEstadoService;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Data
@AllArgsConstructor
@RestController
@RequestMapping(path = "/v1/estados", produces = MediaType.APPLICATION_JSON_VALUE)
public class EstadoController implements EstadoControllerOpenApi {

    private final EstadoRepository estadoRepository;
    private final CadastroEstadoService                  cadastroEstadoService;
    private final EstadoModelAssembler                   modelAssembler;
    private final InputDisassembler<EstadoInput, Estado> inputDisassembler;

    @CheckSecurity.Estados.PodeConsultar
    @GetMapping
    @Override
    public CollectionModel<EstadoModel> listar(){
	List<Estado> estados = estadoRepository.findAll();
	return modelAssembler.toCollectionModel(estados);
    }

    @CheckSecurity.Estados.PodeConsultar
    @GetMapping("/{estadoId}")
    @Override
    public EstadoModel buscar(@PathVariable Long estadoId) {
	Estado estado = cadastroEstadoService.buscaPorId(estadoId);
	return modelAssembler.toModel(estado);
    }

    @CheckSecurity.Estados.PodeEditar
    @PostMapping
    @Override
    public EstadoModel adicionar(@RequestBody @Valid EstadoInput estadoInput) {
	Estado estado = inputDisassembler.toDomainObject(estadoInput, Estado.class);
	cadastroEstadoService.salvar(estado);
	ResourceUriHelper.addUriInResponseHeader(estado.getId());
	return modelAssembler.toModel(estado);
    }

    @CheckSecurity.Estados.PodeEditar
    @PutMapping("/{estadoId}")
    @Override
    public EstadoModel atualizar(@PathVariable Long estadoId, @RequestBody @Valid EstadoInput estadoInput) {
        try {
	    Estado estado = cadastroEstadoService.buscaPorId(estadoId);
	    inputDisassembler.copyToDomainObject(estadoInput, estado);
	    cadastroEstadoService.salvar(estado);
	    return modelAssembler.toModel(estado);
	} catch (RecursoNaoEncontradoException e) {
            throw new NegocioException(e.getMessage(), e);
	}
    }

    @CheckSecurity.Estados.PodeEditar
    @DeleteMapping("/{estadoId}")
    @Override
    public ResponseEntity<?> remover(@PathVariable Long estadoId) {
	this.cadastroEstadoService.excluir(estadoId);
	return ResponseEntity.noContent().build();
    }

}

