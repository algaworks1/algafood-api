package com.algaworks.algafood.infrastructure.service.email;

import com.algaworks.algafood.domain.service.EnvioEmailService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

@Component
public class TemplateEmailComponent {

    private final Configuration freemarkerConfig;

    public TemplateEmailComponent(Configuration freemarkerConfig) {
	this.freemarkerConfig = freemarkerConfig;
    }

    public String processarTemplate(EnvioEmailService.Mensagem mensagem) {
	try {
	    Template template = freemarkerConfig.getTemplate(mensagem.getCorpo());
	    return FreeMarkerTemplateUtils.processTemplateIntoString(template, mensagem.getVariaveis());
	} catch (Exception e) {
	    throw new EmailException("Não foi possível montar o template do e-mail", e);
	}
    }
}
