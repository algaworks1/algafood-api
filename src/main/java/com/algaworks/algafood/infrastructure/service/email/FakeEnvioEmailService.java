package com.algaworks.algafood.infrastructure.service.email;

import com.algaworks.algafood.domain.service.EnvioEmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class FakeEnvioEmailService implements EnvioEmailService {

    private final TemplateEmailComponent templateEmailComponent;

    public FakeEnvioEmailService(TemplateEmailComponent templateEmailComponent) {
	this.templateEmailComponent = templateEmailComponent;
    }

    @Override
    public void enviar(Mensagem mensagem) {
	String corpo = templateEmailComponent.processarTemplate(mensagem);
	log.info("[FAKE E-MAIL] Para: {}\n{}", mensagem.getDestinatarios(), corpo);
    }
}
