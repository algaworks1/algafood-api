package com.algaworks.algafood.infrastructure.service.report;

import com.algaworks.algafood.api.v1.model.filter.VendaDiariaFilter;
import com.algaworks.algafood.domain.service.VendaDiariaQueryService;
import com.algaworks.algafood.domain.service.VendaDiariaReportService;
import java.util.HashMap;
import java.util.Locale;
import lombok.AllArgsConstructor;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class PdfVendaDiariaReport implements VendaDiariaReportService {

    private final VendaDiariaQueryService vendaDiariaQueryService;

    @Override
    public byte[] consultarVendasDiarias(VendaDiariaFilter filter, String timeOffSet) {

	try {
	    var inputStream = this.getClass().getResourceAsStream("/relatorios/vendas-diarias.jasper");

	    var parametros = new HashMap<String, Object>();
	    parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));

	    var vendasDiarias = vendaDiariaQueryService.consultarVendasDiarias(filter, timeOffSet);
	    var dataSource = new JRBeanCollectionDataSource(vendasDiarias);
	    var jasperPrint = JasperFillManager.fillReport(inputStream, parametros, dataSource);
	    return JasperExportManager.exportReportToPdf(jasperPrint);
	} catch (Exception e) {
	    throw new ReportException("Não foi possível emitir relatório de vendas diárias", e);
	}
    }
}
