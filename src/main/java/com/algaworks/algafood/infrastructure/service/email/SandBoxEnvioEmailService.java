package com.algaworks.algafood.infrastructure.service.email;

import com.algaworks.algafood.core.email.EmailProperties;
import java.nio.charset.StandardCharsets;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class SandBoxEnvioEmailService
		extends SmtpEnvioEmailService {

    private final EmailProperties emailProperties;

    public SandBoxEnvioEmailService(@Autowired(required = false) JavaMailSender mailSender, EmailProperties emailProperties, TemplateEmailComponent templateEmailComponent) {
	super(mailSender, emailProperties, templateEmailComponent);
	this.emailProperties = emailProperties;
    }


    @Override
    public void enviar(Mensagem mensagem) {
	super.enviar(mensagem);
    }

    @Override
    protected MimeMessage criarMimeMessage(Mensagem mensagem) throws MessagingException {
	MimeMessage mimeMessage = super.criarMimeMessage(mensagem);
	MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, StandardCharsets.UTF_8.name());
	helper.setTo(emailProperties.getSandBox().getDestinario());
	return helper.getMimeMessage();
    }
}
