package com.algaworks.algafood.infrastructure.service.email;

import com.algaworks.algafood.core.email.EmailProperties;
import com.algaworks.algafood.domain.service.EnvioEmailService;
import java.nio.charset.StandardCharsets;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class SmtpEnvioEmailService
		implements EnvioEmailService {

    private final JavaMailSender mailSender;
    private final EmailProperties emailProperties;
    private final TemplateEmailComponent templateEmailComponent;

    public SmtpEnvioEmailService(@Autowired(required = false) JavaMailSender mailSender, EmailProperties emailProperties,
				 TemplateEmailComponent templateEmailComponent) {
	this.mailSender = mailSender;
	this.emailProperties = emailProperties;
	this.templateEmailComponent = templateEmailComponent;
    }

    @Override
    public void enviar(Mensagem mensagem) {
	try {
	    MimeMessage mimeMessage = criarMimeMessage(mensagem);
	    mailSender.send(mimeMessage);
	} catch (Exception e) {
	    throw new EmailException("Não foi possível enviar e-mail", e);
	}
    }

    protected MimeMessage criarMimeMessage(Mensagem mensagem) throws MessagingException {
	MimeMessage mimeMessage = mailSender.createMimeMessage();
	MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, StandardCharsets.UTF_8.name());

	helper.setFrom(emailProperties.getRemetente());
	helper.setTo(mensagem.getDestinatarios().toArray(new String[0]));
	helper.setSubject(mensagem.getAssunto());
	String corpo = templateEmailComponent.processarTemplate(mensagem);
	helper.setText(corpo, true);

	return mimeMessage;
    }
}
