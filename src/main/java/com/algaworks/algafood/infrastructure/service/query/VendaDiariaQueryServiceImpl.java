package com.algaworks.algafood.infrastructure.service.query;

import com.algaworks.algafood.api.v1.model.filter.VendaDiariaFilter;
import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.model.StatusPedido;
import com.algaworks.algafood.domain.model.dto.VendaDiaria;
import com.algaworks.algafood.domain.service.VendaDiariaQueryService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;
import org.springframework.stereotype.Repository;

@Repository
public class VendaDiariaQueryServiceImpl implements VendaDiariaQueryService {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public List<VendaDiaria> consultarVendasDiarias(VendaDiariaFilter filter, String timeOffSet) {

	var builder = manager.getCriteriaBuilder();
	var query = builder.createQuery(VendaDiaria.class);
	var root = query.from(Pedido.class);

	var fieldCountId = builder.count(root.get("id"));
	var fieldSumValorTotal = builder.sum(root.get("valorTotal"));

	var functionConvertTimeZoneBR = builder.function("convert_tz", Date.class, root.get("dataCriacao"),
							 builder.literal("+00:00"), builder.literal(timeOffSet));

	var functionDateDataCriacao = builder.function("date", Date.class, functionConvertTimeZoneBR);

	var selection = builder.construct(VendaDiaria.class, functionDateDataCriacao, fieldCountId, fieldSumValorTotal);

	query.select(selection);

	var predicates = new ArrayList<Predicate>();

	if (filter.getRestauranteId() != null) {
	    predicates.add(builder.equal(root.get("restaurante"), filter.getRestauranteId()));
	}

	if (filter.getDataCriacaoInicio() != null) {
	    predicates.add(builder.greaterThanOrEqualTo(root.get("dataCriacao"), filter.getDataCriacaoInicio()));
	}

	if (filter.getDataCriacaoFim() != null) {
	    predicates.add(builder.greaterThanOrEqualTo(root.get("dataCriacao"), filter.getDataCriacaoFim()));
	}

	predicates.add(root.get("status").in(StatusPedido.CONFIRMADO, StatusPedido.ENTREGUE));

	query.where(predicates.toArray(new Predicate[0]));
	query.groupBy(functionDateDataCriacao);

	return manager.createQuery(query).getResultList();
    }
}
