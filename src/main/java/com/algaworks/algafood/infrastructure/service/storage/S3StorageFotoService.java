package com.algaworks.algafood.infrastructure.service.storage;

import com.algaworks.algafood.core.storage.StorageProperties;
import com.algaworks.algafood.domain.service.StorageFotoService;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import java.net.URL;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class S3StorageFotoService implements StorageFotoService {

    private final AmazonS3          amazonS3Client;
    private final StorageProperties storageProperties;

    @Override
    public FotoRecuperada recuperar(String nomeArquivo) {
	String caminhoArquivo = getCaminhoArquivo(nomeArquivo);
	URL url = amazonS3Client.getUrl(storageProperties.getS3().getBucket(), caminhoArquivo);
	return FotoRecuperada.builder().url(url.toString()).build();
    }

    @Override
    public void armazenar(NovaFoto novaFoto) {
        try {
	    String caminhoArquivo = getCaminhoArquivo(novaFoto.getNomeArquivo());
	    var objectMetadata = new ObjectMetadata();
	    objectMetadata.setContentType(novaFoto.getContentType());
	    objectMetadata.setContentLength(novaFoto.getTamanho());

	    var putObjectRequest = new PutObjectRequest(storageProperties.getS3().getBucket(),
							caminhoArquivo, novaFoto.getInputStream(),
						 	objectMetadata)
			    			.withCannedAcl(CannedAccessControlList.PublicRead);

	    amazonS3Client.putObject(putObjectRequest);
	} catch (Exception e) {
            throw new StorageException("Não foi possível enviar arquivo para Amazon S3.", e);
	}
    }

    @Override
    public void remover(String nomeArquivo) {
        try {
	    String caminhoArquivo = getCaminhoArquivo(nomeArquivo);
	    var deleteObjectRequest = new DeleteObjectRequest(storageProperties.getS3().getBucket(), caminhoArquivo);
	    amazonS3Client.deleteObject(deleteObjectRequest);
	} catch (Exception e) {
	    throw new StorageException("Não foi possível excluir arquivo na Amazon S3.", e);
	}
    }

    private String getCaminhoArquivo(String nomeArquivo) {
	return String.format("%s/%s", storageProperties.getS3().getDiretorioFotos(), nomeArquivo);
    }
}
