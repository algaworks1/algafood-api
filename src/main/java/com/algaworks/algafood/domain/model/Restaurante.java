package com.algaworks.algafood.domain.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Restaurante {

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String nome;

    @Column(name = "taxa_frete", nullable = false)
    private BigDecimal taxaFrete;

    @ManyToOne
    @JoinColumn(name = "cozinha_id", nullable = false)
    private Cozinha cozinha;

    @Embedded
    private Endereco endereco;

    @CreationTimestamp
    @Column(nullable = false, columnDefinition = "datetime")
    private OffsetDateTime dataCadastro;

    @UpdateTimestamp
    @Column(nullable = false, columnDefinition = "datetime")
    private OffsetDateTime dataAtualizacao;

    @Column(nullable = false)
    private Boolean ativo = Boolean.TRUE;

    @Column(nullable = false)
    private Boolean aberto = Boolean.FALSE;

    @OneToMany(mappedBy = "restaurante")
    private List<Produto> produtos = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "restaurante_forma_pagamento",
		    joinColumns = @JoinColumn(name = "restaurante_id"),
		    inverseJoinColumns = @JoinColumn(name = "forma_pagamento_id"))
    private Set<FormaPagamento> formasPagamento = new HashSet<>();


    @ManyToMany
    @JoinTable(name = "restaurante_usuario_responsavel",
		    joinColumns = @JoinColumn(name = "restaurante_id"),
		    inverseJoinColumns = @JoinColumn(name = "usuario_id"))
    private Set<Usuario> responsaveis = new HashSet<>();

    public void ativar() {
	this.setAtivo(Boolean.TRUE);
    }

    public void inativar() {
	this.setAtivo(Boolean.FALSE);
    }

    public boolean associarFormaPagamento(FormaPagamento formaPagamento) {
        return getFormasPagamento().add(formaPagamento);
    }

    public boolean desassociarFormaPagamento(FormaPagamento formaPagamento) {
	return getFormasPagamento().remove(formaPagamento);
    }

    public boolean adicionarProduto(Produto produto) {
        return getProdutos().add(produto);
    }

    public boolean removerProduto(Produto produto) {
	return getProdutos().remove(produto);
    }

    public void abrir() {
        this.setAberto(Boolean.TRUE);
    }

    public void fechar() {
	this.setAberto(Boolean.FALSE);
    }

    public boolean associarUsuarioResponsavel(Usuario usuario) {
        return getResponsaveis().add(usuario);
    }

    public boolean desassociarUsuarioResponsavel(Usuario usuario) {
	return getResponsaveis().remove(usuario);
    }

    public boolean aceitaFormaPagamento(FormaPagamento formaPagamento) {
	return getFormasPagamento().contains(formaPagamento);
    }

    public boolean naoAceitaFormaPagamento(FormaPagamento formaPagamento) {
	return !aceitaFormaPagamento(formaPagamento);
    }

    public boolean isAberto() {
	return this.aberto;
    }

    public boolean isFechado() {
	return !isAberto();
    }

    public boolean isInativo() {
	return !isAtivo();
    }

    public boolean isAtivo() {
	return this.ativo;
    }

    public boolean aberturaPermitida() {
	return isAtivo() && isFechado();
    }

    public boolean ativacaoPermitida() {
	return isInativo();
    }

    public boolean inativacaoPermitida() {
	return isAtivo();
    }

    public boolean fechamentoPermitido() {
	return isAberto();
    }
}
