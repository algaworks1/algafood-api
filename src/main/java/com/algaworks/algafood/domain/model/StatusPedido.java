package com.algaworks.algafood.domain.model;

import java.util.Arrays;
import java.util.List;

public enum StatusPedido {

    CRIADO("Criado"),
    CONFIRMADO("Confimado", CRIADO),
    ENTREGUE("Entregue", CONFIRMADO),
    CANCELADO("Cancelado", CRIADO, CONFIRMADO);

    private final String descricao;
    private final List<StatusPedido> statusPedidosAnteriores;

    StatusPedido(String descricao, StatusPedido... statusPedidosAnteriores) {
	this.descricao = descricao;
	this.statusPedidosAnteriores = Arrays.asList(statusPedidosAnteriores);
    }

    public String getDescricao() {
	return descricao;
    }

    public boolean naoPodeAlterarPara(StatusPedido novoStatus){
	return !novoStatus.statusPedidosAnteriores.contains(this);
    }

    public boolean podeAlterarPara(StatusPedido novoStatus){
	return !naoPodeAlterarPara(novoStatus);
    }
}
