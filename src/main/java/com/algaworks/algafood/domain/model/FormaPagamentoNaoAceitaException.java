package com.algaworks.algafood.domain.model;

import com.algaworks.algafood.domain.exception.NegocioException;

public class FormaPagamentoNaoAceitaException
		extends NegocioException {

    public FormaPagamentoNaoAceitaException(String mensagem) {
	super(String.format("Forma de pagamento '%s' não é aceita por esse restaurante.", mensagem));
    }
}
