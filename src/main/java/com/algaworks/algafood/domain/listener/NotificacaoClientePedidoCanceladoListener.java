package com.algaworks.algafood.domain.listener;

import com.algaworks.algafood.domain.event.PedidoCanceladoEvent;
import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.service.EnvioEmailService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@AllArgsConstructor
@Component
public class NotificacaoClientePedidoCanceladoListener {

    private final EnvioEmailService envioEmailService;

    @TransactionalEventListener
    public void aoCancelarPedido(PedidoCanceladoEvent event) {
	Pedido pedido = event.getPedido();

	var mensagem = EnvioEmailService.Mensagem.builder()
			.assunto(pedido.getRestaurante().getNome() + " - Pedido cancelado")
			.corpo("emails/pedido-cancelado.html")
			.variavel("pedido", pedido)
			.destinatario(pedido.getCliente().getEmail())
			.build();

	envioEmailService.enviar(mensagem);
    }

}
