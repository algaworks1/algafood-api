package com.algaworks.algafood.domain.exception;

public class AlteracaoStatusException
		extends NegocioException {

    private static final long serialVersionUID = 1L;

    public AlteracaoStatusException(String mensagem) {
	super(mensagem);
    }

    public AlteracaoStatusException(String codigoPedido, String statusAtual, String statusSolicitado) {
	this(String.format("Status do pedido %s não pode ser alterado de %s para %s", codigoPedido, statusAtual, statusSolicitado));
    }
}
