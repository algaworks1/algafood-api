package com.algaworks.algafood.domain.exception;

public abstract class RecursoNaoEncontradoException
		extends NegocioException {

    private static final long serialVersionUID = 1L;

    public RecursoNaoEncontradoException(String mensagem) {
	super(mensagem);
    }
}
