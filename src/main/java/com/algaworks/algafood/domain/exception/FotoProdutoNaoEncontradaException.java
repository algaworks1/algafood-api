package com.algaworks.algafood.domain.exception;

public class FotoProdutoNaoEncontradaException
		extends RecursoNaoEncontradoException {

    private static final long serialVersionUID = 1L;

    public FotoProdutoNaoEncontradaException(String mensagem) {
	super(mensagem);
    }

    public FotoProdutoNaoEncontradaException(Long restauranteId, Long produtoId) {
	this(String.format("Não existe um cadastro de foto do produto com código %d e restaurante %d", produtoId, restauranteId));
    }
}
