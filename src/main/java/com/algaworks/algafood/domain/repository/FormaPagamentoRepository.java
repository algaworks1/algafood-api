package com.algaworks.algafood.domain.repository;

import com.algaworks.algafood.domain.model.FormaPagamento;
import java.time.OffsetDateTime;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FormaPagamentoRepository
		extends CustomJpaRepository<FormaPagamento, Long> {

    @Query("select max(fp.dataAtualizacao) from FormaPagamento fp")
    Optional<OffsetDateTime> getDataUltimaAtualizacao();

}
