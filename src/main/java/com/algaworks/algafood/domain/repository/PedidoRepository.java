package com.algaworks.algafood.domain.repository;

import com.algaworks.algafood.domain.model.Pedido;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PedidoRepository extends CustomJpaRepository<Pedido, Long>, JpaSpecificationExecutor<Pedido> {

    Optional<Pedido> findByCodigo(String codigo);

    boolean isPedidoGerenciadoPorRestaurante(String codigoPedido, Long usuarioId);
}
