package com.algaworks.algafood.domain.service;

import com.algaworks.algafood.domain.exception.CidadeNaoEncontradaException;
import com.algaworks.algafood.domain.exception.EntidadeEmUsoException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.repository.CidadeRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@AllArgsConstructor
@Service
public class CadastroCidadeService {

    private static final String MSG_CIDADE_EM_USO = "Cidade com código %d não pode ser removido, pois está em uso";

    private final CidadeRepository cidadeRepository;
    private final CadastroEstadoService cadastroEstadoService;

    @Transactional
    public Cidade salvar(Cidade cidade) {
	Long estadoId = cidade.getEstado().getId();
	Estado estado = cadastroEstadoService.buscaPorId(estadoId);
	cidade.setEstado(estado);
	return this.cidadeRepository.save(cidade);
    }

    @Transactional
    public void excluir(Long id) {
	try {
	    cidadeRepository.deleteById(id);
	    cidadeRepository.flush();
	} catch (EmptyResultDataAccessException e) {
	    throw new CidadeNaoEncontradaException(id);
	} catch (DataIntegrityViolationException e) {
	    throw new EntidadeEmUsoException(String.format(MSG_CIDADE_EM_USO, id));
    	}
    }

    public Cidade buscaPorId(Long cidadeId) {
	return this.cidadeRepository.findById(cidadeId).orElseThrow(() -> new CidadeNaoEncontradaException(cidadeId));
    }

}
