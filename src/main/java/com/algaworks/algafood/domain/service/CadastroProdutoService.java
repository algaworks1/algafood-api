package com.algaworks.algafood.domain.service;

import com.algaworks.algafood.domain.exception.ProdutoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Produto;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.ProdutoRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@AllArgsConstructor
@Service
public class CadastroProdutoService {

    private final ProdutoRepository produtoRepository;
    private final CadastroRestauranteService cadastroRestauranteService;


    @Transactional
    public Produto salvar(Long restauranteId, Produto produto) {
	Restaurante restaurante = cadastroRestauranteService.buscaPorId(restauranteId);
	produto.setRestaurante(restaurante);
	return produtoRepository.save(produto);
    }

    public List<Produto> buscaTodosPorRestaurante(Long restauranteId) {
	Restaurante restaurante = cadastroRestauranteService.buscaPorId(restauranteId);
	return restaurante.getProdutos();
    }

    public List<Produto> buscaAtivosPorRestaurante(Long restauranteId) {
	Restaurante restaurante = cadastroRestauranteService.buscaPorId(restauranteId);
	return produtoRepository.findAllByAtivoIsTrueAndRestaurante(restaurante);
    }

    public Produto buscaPorId(Long restauranteId, Long produtoId) {
	Restaurante restaurante = cadastroRestauranteService.buscaPorId(restauranteId);
	return produtoRepository.findByIdAndRestaurante(produtoId, restaurante).orElseThrow(() -> new ProdutoNaoEncontradoException(restauranteId, produtoId));
    }

}
