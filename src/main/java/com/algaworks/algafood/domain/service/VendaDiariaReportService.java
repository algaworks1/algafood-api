package com.algaworks.algafood.domain.service;

import com.algaworks.algafood.api.v1.model.filter.VendaDiariaFilter;
import org.springframework.stereotype.Service;

@Service
public interface VendaDiariaReportService {

    byte[] consultarVendasDiarias(VendaDiariaFilter filter, String timeOffSet);

}
