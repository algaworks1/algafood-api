package com.algaworks.algafood.domain.service;

import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.repository.PedidoRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@AllArgsConstructor
@Service
public class FluxoPedidoService {

    private final EmissaoPedidoService emissaoPedidoService;
    private final PedidoRepository     pedidoRepository;

    @Transactional
    public void confirmar(String codigoPedido) {
	Pedido pedido = emissaoPedidoService.buscaPorCodigo(codigoPedido);
	pedido.confirmar();
	pedidoRepository.save(pedido);
    }

    @Transactional
    public void entregar(String codigoPedido) {
	Pedido pedido = emissaoPedidoService.buscaPorCodigo(codigoPedido);
	pedido.entregar();
    }

    @Transactional
    public void cancelar(String codigoPedido) {
	Pedido pedido = emissaoPedidoService.buscaPorCodigo(codigoPedido);
	pedido.cancelar();
	pedidoRepository.save(pedido);
    }
}
