package com.algaworks.algafood.domain.service;

import com.algaworks.algafood.domain.exception.FotoProdutoNaoEncontradaException;
import com.algaworks.algafood.domain.model.FotoProduto;
import com.algaworks.algafood.domain.repository.ProdutoRepository;
import com.algaworks.algafood.domain.service.StorageFotoService.FotoRecuperada;
import java.io.InputStream;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.algaworks.algafood.domain.service.StorageFotoService.NovaFoto;

@AllArgsConstructor
@Service
public class CatalogoFotoProdutoService {

    private final ProdutoRepository produtoRepository;
    private final StorageFotoService storageFotoService;

    @Transactional
    public FotoProduto salvar(FotoProduto fotoProduto, InputStream dadosFoto) {

	Long restauranteId = fotoProduto.getRestauranteId();
	Long produtoId = fotoProduto.getProduto().getId();

	Optional<FotoProduto> fotoProdutoOpt = produtoRepository.findFotoById(restauranteId, produtoId);
	fotoProdutoOpt.ifPresent(foto -> {
	    produtoRepository.delete(foto);
	    storageFotoService.remover(foto.getNomeArquivo());
	});

	String nomeArquivoComUUID = storageFotoService.gerarNomeArquivo(fotoProduto.getNomeArquivo());
	fotoProduto.setNomeArquivo(nomeArquivoComUUID);
	FotoProduto fotoSalva = produtoRepository.save(fotoProduto);
	produtoRepository.flush();

	NovaFoto novaFoto = NovaFoto.builder()
					.nomeArquivo(nomeArquivoComUUID)
					.contentType(fotoProduto.getContentType())
					.inputStream(dadosFoto)
					.tamanho(fotoProduto.getTamanho())
					.build();

	storageFotoService.armazenar(novaFoto);

	return fotoSalva;
    }

    public FotoRecuperada recuperar(String nomeArquivo) {
	return storageFotoService.recuperar(nomeArquivo);
    }

    @Transactional
    public void excluir(Long restauranteId, Long produtoId) {
	FotoProduto foto = buscarOuFalhar(restauranteId, produtoId);

	produtoRepository.delete(foto);
	produtoRepository.flush();

	storageFotoService.remover(foto.getNomeArquivo());
    }

    public FotoProduto buscarOuFalhar(Long restauranteId, Long produtoId) {
	return produtoRepository.findFotoById(restauranteId, produtoId)
			.orElseThrow(() -> new FotoProdutoNaoEncontradaException(restauranteId, produtoId));
    }
}
