package com.algaworks.algafood.domain.service;

import java.io.InputStream;
import java.util.UUID;
import lombok.Builder;
import lombok.Getter;
import org.springframework.stereotype.Service;

@Service
public interface StorageFotoService {

    FotoRecuperada recuperar(String nomeArquivo);

    void armazenar(NovaFoto novaFoto);

    void remover(String nomeArquivo);

    default String gerarNomeArquivo(String nomeOriginal) {
	return UUID.randomUUID().toString() + "_" + nomeOriginal;
    }

    @Builder
    @Getter
    class NovaFoto {
        private InputStream inputStream;
        private String nomeArquivo;
        private String contentType;
        private Long tamanho;
    }

    @Builder
    @Getter
    class FotoRecuperada {
        private InputStream inputStream;
        private String url;

        public boolean temInputStream() {
            return inputStream != null;
	}

	public boolean temInputUrl() {
	    return url != null;
	}
    }
}
