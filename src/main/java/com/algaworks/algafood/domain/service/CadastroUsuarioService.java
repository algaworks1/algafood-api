package com.algaworks.algafood.domain.service;

import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.SenhaInvalidaException;
import com.algaworks.algafood.domain.exception.UsuarioNaoEncontradaException;
import com.algaworks.algafood.domain.model.Grupo;
import com.algaworks.algafood.domain.model.Usuario;
import com.algaworks.algafood.domain.repository.UsuarioRepository;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@AllArgsConstructor
@Service
public class CadastroUsuarioService {

    private static final String MSG_SENHA_INVALIDA = "Senha atual informada não coincide com a senha do usuário %s";

    private final UsuarioRepository usuarioRepository;
    private final CadastroGrupoService cadastroGrupoService;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public Usuario salvar(Usuario usuario) {
	usuarioRepository.detach(usuario);
	Optional<Usuario> usuarioEncontrado = usuarioRepository.findByEmail(usuario.getEmail());

	if (usuarioEncontrado.isPresent() && !usuarioEncontrado.get().equals(usuario)) {
	    throw new NegocioException(String.format("Já existe um usuário cadastrado para email %s", usuario.getEmail()));
	}

	if (usuario.isNovo()) {
	    usuario.setSenha(passwordEncoder.encode(usuario.getSenha()));
	}

	return usuarioRepository.save(usuario);
    }

    @Transactional
    public void alterarSenha(Long usuarioId, String senhaAtual, String novaSenha) {
	Usuario usuario = buscaPorId(usuarioId);

	if (!passwordEncoder.matches(senhaAtual, usuario.getSenha())) {
	    throw new SenhaInvalidaException(String.format(MSG_SENHA_INVALIDA, usuario.getNome()));
	}

	usuario.setSenha(passwordEncoder.encode(novaSenha));
    }

    @Transactional
    public void associarGrupo(Long usuarioId, Long grupoId) {
	Usuario usuario = buscaPorId(usuarioId);
	Grupo grupo = cadastroGrupoService.buscaPorId(grupoId);
	usuario.adicionarGrupo(grupo);
    }

    @Transactional
    public void desassociarGrupo(Long usuarioId, Long grupoId) {
	Usuario usuario = buscaPorId(usuarioId);
	Grupo grupo = cadastroGrupoService.buscaPorId(grupoId);
	usuario.removerGrupo(grupo);
    }

    public Usuario buscaPorId(Long usuarioId) {
	return usuarioRepository.findById(usuarioId).orElseThrow(() -> new UsuarioNaoEncontradaException(usuarioId));
    }
}
