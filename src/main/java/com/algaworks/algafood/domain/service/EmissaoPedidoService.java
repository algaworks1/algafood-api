package com.algaworks.algafood.domain.service;

import com.algaworks.algafood.domain.exception.PedidoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.FormaPagamento;
import com.algaworks.algafood.domain.model.FormaPagamentoNaoAceitaException;
import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.model.Produto;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.model.Usuario;
import com.algaworks.algafood.domain.repository.PedidoRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@AllArgsConstructor
@Service
public class EmissaoPedidoService {

    private final PedidoRepository              pedidoRepository;
    private final CadastroRestauranteService    cadastroRestauranteService;
    private final CadastroFormaPagamentoService cadastroFormaPagamentoService;
    private final CadastroProdutoService        cadastroProdutoService;
    private final CadastroCidadeService         cadastroCidadeService;
    private final CadastroUsuarioService        cadastroUsuarioService;

    @Transactional
    public Pedido emitir(Pedido pedido) {
	Restaurante restaurante = cadastroRestauranteService.buscaPorId(pedido.getRestaurante().getId());
	FormaPagamento formaPagamento = cadastroFormaPagamentoService.buscaPorId(pedido.getFormaPagamento().getId());

	if (restaurante.naoAceitaFormaPagamento(formaPagamento)) {
	    throw new FormaPagamentoNaoAceitaException(formaPagamento.getDescricao());
	}

	pedido.getItens().forEach(item -> {
	    Produto produto = cadastroProdutoService.buscaPorId(restaurante.getId(), item.getProduto().getId());
	    item.setProduto(produto);
	    item.setPrecoUnitario(produto.getPreco());
	});

	Cidade cidade = cadastroCidadeService.buscaPorId(pedido.getEnderecoEntrega().getCidade().getId());
	Usuario usuario = cadastroUsuarioService.buscaPorId(pedido.getCliente().getId());

	pedido.setRestaurante(restaurante);
	pedido.setFormaPagamento(formaPagamento);
	pedido.getEnderecoEntrega().setCidade(cidade);
	pedido.setCliente(usuario);

	pedido.definirFrete();
	pedido.atribuirPedidoAosItens();
	pedido.calcularValorTotal();

	return pedidoRepository.save(pedido);
    }

    public Pedido buscaPorCodigo(String codigoPedido) {
	return pedidoRepository.findByCodigo(codigoPedido).orElseThrow(() -> new PedidoNaoEncontradoException(codigoPedido));
    }
}
