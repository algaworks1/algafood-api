package com.algaworks.algafood.domain.service;

import com.algaworks.algafood.domain.exception.EntidadeEmUsoException;
import com.algaworks.algafood.domain.exception.EstadoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.repository.EstadoRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@AllArgsConstructor
@Service
public class CadastroEstadoService {

    private static final String MSG_ESTADO_EM_USO = "Estado com código %d não pode ser removido, pois está em uso";

    private final EstadoRepository estadoRepository;

    public Estado salvar(Estado estado) {
	return this.estadoRepository.save(estado);
    }

    @Transactional
    public void excluir(Long id) {
	try {
	    estadoRepository.deleteById(id);
	    estadoRepository.flush();
	} catch (EmptyResultDataAccessException e) {
	    throw new EstadoNaoEncontradoException(id);
	} catch (DataIntegrityViolationException e) {
	    throw new EntidadeEmUsoException(String.format(MSG_ESTADO_EM_USO, id));
	}
    }

    public Estado buscaPorId(Long estadoId) {
	return this.estadoRepository.findById(estadoId).orElseThrow(() -> new EstadoNaoEncontradoException(estadoId));
    }

}
