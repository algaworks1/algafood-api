package com.algaworks.algafood.domain.service;

import com.algaworks.algafood.domain.exception.RestauranteNaoEncontradoException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.model.FormaPagamento;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.model.Usuario;
import com.algaworks.algafood.domain.repository.RestauranteRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@AllArgsConstructor
@Service
public class CadastroRestauranteService {

    private final RestauranteRepository         restauranteRepository;
    private final CadastroCozinhaService        cadastroCozinha;
    private final CadastroCidadeService         cadastroCidadeService;
    private final CadastroFormaPagamentoService cadastroFormaPagamentoService;
    private final CadastroUsuarioService        cadastroUsuarioService;

    @Transactional
    public Restaurante salvar(Restaurante restaurante) {
	Long cozinhaId = restaurante.getCozinha().getId();
	Long cidadeId = restaurante.getEndereco().getCidade().getId();

	Cozinha cozinha = cadastroCozinha.buscaPorId(cozinhaId);
	Cidade cidade = cadastroCidadeService.buscaPorId(cidadeId);

	restaurante.setCozinha(cozinha);
	restaurante.getEndereco().setCidade(cidade);

	return restauranteRepository.save(restaurante);
    }

    @Transactional
    public void ativar(Long restauranteId) {
	Restaurante restaurante = buscaPorId(restauranteId);
	restaurante.ativar();
    }

    @Transactional
    public void ativar(List<Long> restauranteIds) {
        restauranteIds.forEach(this::ativar);
    }

    @Transactional
    public void inativar(Long restauranteId) {
	Restaurante restaurante = buscaPorId(restauranteId);
	restaurante.inativar();
    }

    @Transactional
    public void inativar(List<Long> restauranteIds) {
        restauranteIds.forEach(this::inativar);
    }

    @Transactional
    public void abrir(Long restauranteId) {
	Restaurante restaurante = buscaPorId(restauranteId);
	restaurante.abrir();
    }

    @Transactional
    public void fechar(Long restauranteId) {
	Restaurante restaurante = buscaPorId(restauranteId);
	restaurante.fechar();
    }

    @Transactional
    public void associarFormaPagamento(Long restauranteId, Long formaDePagamentoId) {
	Restaurante restaurante = buscaPorId(restauranteId);
	FormaPagamento formaPagamento = cadastroFormaPagamentoService.buscaPorId(formaDePagamentoId);
	restaurante.associarFormaPagamento(formaPagamento);
    }

    @Transactional
    public void desassociarFormaPagamento(Long restauranteId, Long formaDePagamentoId) {
	Restaurante restaurante = buscaPorId(restauranteId);
	FormaPagamento formaPagamento = cadastroFormaPagamentoService.buscaPorId(formaDePagamentoId);
	restaurante.desassociarFormaPagamento(formaPagamento);
    }

    @Transactional
    public void associarPermissao(Long restauranteId, Long usuarioId) {
	Restaurante restaurante = buscaPorId(restauranteId);
	Usuario usuario = cadastroUsuarioService.buscaPorId(usuarioId);
	restaurante.associarUsuarioResponsavel(usuario);
    }

    @Transactional
    public void desassociarPermissao(Long restauranteId, Long usuarioId) {
	Restaurante restaurante = buscaPorId(restauranteId);
	Usuario usuario = cadastroUsuarioService.buscaPorId(usuarioId);
	restaurante.desassociarUsuarioResponsavel(usuario);
    }

    public Restaurante buscaPorId(Long restauranteId) {
	return restauranteRepository.findById(restauranteId).orElseThrow(() -> new RestauranteNaoEncontradoException(restauranteId));
    }
}
