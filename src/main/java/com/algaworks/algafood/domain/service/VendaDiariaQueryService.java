package com.algaworks.algafood.domain.service;

import com.algaworks.algafood.api.v1.model.filter.VendaDiariaFilter;
import com.algaworks.algafood.domain.model.dto.VendaDiaria;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface VendaDiariaQueryService {

    List<VendaDiaria> consultarVendasDiarias(VendaDiariaFilter filter, String timeOffSet);

}
