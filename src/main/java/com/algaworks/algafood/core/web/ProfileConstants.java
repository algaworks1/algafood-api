package com.algaworks.algafood.core.web;

public final class ProfileConstants {

    public static final String NOT_EQUALS_TEST = "!test";
    public static final String TEST            = "test";

    private ProfileConstants() {
    }
}
