package com.algaworks.algafood.core.validation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.validation.BeanPropertyBindingResult;

@Getter
@AllArgsConstructor
public class ValidacaoException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final BeanPropertyBindingResult bindingResult;

}
