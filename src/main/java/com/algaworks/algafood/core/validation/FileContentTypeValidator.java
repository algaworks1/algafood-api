package com.algaworks.algafood.core.validation;

import java.util.Set;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.web.multipart.MultipartFile;

public class FileContentTypeValidator
		implements ConstraintValidator<FileContentType, MultipartFile> {

    private Set<String> allowedMediaTypes;

    @Override
    public void initialize(FileContentType constraintAnnotation) {
	this.allowedMediaTypes = Set.of(constraintAnnotation.allowed());
    }

    @Override
    public boolean isValid(MultipartFile multipartFile, ConstraintValidatorContext context) {
	return multipartFile == null || this.allowedMediaTypes.contains(multipartFile.getContentType());
    }
}
