package com.algaworks.algafood.core.storage;

import com.algaworks.algafood.domain.service.StorageFotoService;
import com.algaworks.algafood.infrastructure.service.storage.LocalStorageFotoService;
import com.algaworks.algafood.infrastructure.service.storage.S3StorageFotoService;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@AllArgsConstructor
@Configuration
public class StorageConfig {

    private final StorageProperties storageProperties;

    @ConditionalOnProperty(name = "algafood.storage.tipo", havingValue = "s3")
    @Bean
    public AmazonS3 amazonS3() {
	var credentials = new BasicAWSCredentials(storageProperties.getS3().getIdChaveAcesso(),
						  storageProperties.getS3().getChaveAcessoSecreta());

	return AmazonS3ClientBuilder.standard()
			.withCredentials(new AWSStaticCredentialsProvider(credentials))
			.withRegion(storageProperties.getS3().getRegiao())
			.build();
    }

    @Bean
    public StorageFotoService storageFotoService() {
        if (StorageProperties.Tipo.LOCAL.equals(storageProperties.getTipo())) {
            return new LocalStorageFotoService(storageProperties);
	} else {
	    return new S3StorageFotoService(amazonS3(), storageProperties);
	}
    }

}
