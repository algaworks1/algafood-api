package com.algaworks.algafood.core.storage;

import com.amazonaws.regions.Regions;
import java.nio.file.Path;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties("algafood.storage")
public class StorageProperties {

    private Local local = new Local();
    private S3 s3 = new S3();
    private Tipo tipo = Tipo.LOCAL;

    @Getter
    @Setter
    public static class Local {
	private Path diretorioFotos;
    }

    @Getter
    @Setter
    public static class S3 {
	private String  idChaveAcesso;
	private String  chaveAcessoSecreta;
	private String  bucket;
	private Regions regiao;
	private String  diretorioFotos;
    }

    @Getter
    public enum Tipo {
        LOCAL, S3
    }

}

