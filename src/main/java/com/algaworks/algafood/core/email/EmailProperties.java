package com.algaworks.algafood.core.email;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties("algafood.email")
public class EmailProperties {

    @Getter
    @Setter
    private String remetente;
    private Implementacao impl = Implementacao.FAKE;
    private SandBox sandBox = new SandBox();

    @Getter
    public enum Implementacao {
	FAKE, SANDBOX, SMTP
    }

    @Getter
    @Setter
    public static class SandBox {

	private String destinario;
    }

}

