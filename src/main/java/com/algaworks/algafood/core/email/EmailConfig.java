package com.algaworks.algafood.core.email;

import com.algaworks.algafood.domain.service.EnvioEmailService;
import com.algaworks.algafood.infrastructure.service.email.FakeEnvioEmailService;
import com.algaworks.algafood.infrastructure.service.email.SandBoxEnvioEmailService;
import com.algaworks.algafood.infrastructure.service.email.SmtpEnvioEmailService;
import com.algaworks.algafood.infrastructure.service.email.TemplateEmailComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;

@Configuration
public class EmailConfig {

    private final JavaMailSender mailSender;
    private final EmailProperties emailProperties;
    private final TemplateEmailComponent templateEmailComponent;

    public EmailConfig(@Autowired(required = false) JavaMailSender mailSender, EmailProperties emailProperties, TemplateEmailComponent templateEmailComponent) {
	this.mailSender = mailSender;
	this.emailProperties = emailProperties;
	this.templateEmailComponent = templateEmailComponent;
    }

    @Bean
    public EnvioEmailService envioEmailService() {
        switch (emailProperties.getImpl()) {
	    case SMTP:
		return new SmtpEnvioEmailService(mailSender, emailProperties, templateEmailComponent);
	    case SANDBOX:
		return new SandBoxEnvioEmailService(mailSender, emailProperties, templateEmailComponent);
	    case FAKE:
		return new FakeEnvioEmailService(templateEmailComponent);
	    default:
	        return null;
	}
    }

}
