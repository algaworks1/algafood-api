package com.algaworks.algafood.core.security.authorizationserver;

import com.algaworks.algafood.domain.model.Usuario;
import java.util.Set;
import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

@Getter
public class AuthUser extends User {

    private final Long userId;
    private final String fullName;

    public AuthUser(Usuario usuario, Set<SimpleGrantedAuthority> authorities) {
	super(usuario.getEmail(), usuario.getSenha(), authorities);
	this.userId = usuario.getId();
	this.fullName = usuario.getNome();
    }
}
