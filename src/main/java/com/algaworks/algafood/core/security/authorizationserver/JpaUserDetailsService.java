package com.algaworks.algafood.core.security.authorizationserver;

import com.algaworks.algafood.domain.model.Usuario;
import com.algaworks.algafood.domain.repository.UsuarioRepository;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class JpaUserDetailsService implements UserDetailsService {

    private final UsuarioRepository usuarioRepository;

    public JpaUserDetailsService(UsuarioRepository usuarioRepository) {
	this.usuarioRepository = usuarioRepository;
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	Usuario usuario = usuarioRepository.findByEmail(username)
					    .orElseThrow(() -> new UsernameNotFoundException("Usuário " + username + " não foi encontrado"));

	Set<SimpleGrantedAuthority> authorities = usuario.getGrupos().stream()
								    .flatMap(grupo -> grupo.getPermissoes().stream())
								    .map(permissao -> new SimpleGrantedAuthority(permissao.getNome()))
								    .collect(Collectors.toSet());
	return new AuthUser(usuario, authorities);
    }
}
