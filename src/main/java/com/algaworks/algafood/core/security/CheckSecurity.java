package com.algaworks.algafood.core.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

public @interface CheckSecurity {

    @interface Cozinhas {

	@PreAuthorize("hasAuthority('SCOPE_READ') and isAuthenticated()")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeConsultar { }


	@PreAuthorize("@algaSecurity.podeConsultarCozinhas()")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeEditar { }
    }

    @interface Restaurantes {

	@PreAuthorize("@algaSecurity.podeGerenciarCadastroRestaurantes()")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeGerenciarCadastro { }

	@PreAuthorize("@algaSecurity.podeGerenciarFuncionamentoRestaurantes(#restauranteId)")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeGerenciarFuncionamento { }

	@PreAuthorize("@algaSecurity.podeConsultarRestaurantes()")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeConsultar { }
    }

    @interface Pedidos {

	@PreAuthorize("hasAuthority('SCOPE_READ') and isAuthenticated()")
	@PostAuthorize("hasAuthority('CONSULTAR_PEDIDOS') or "
			+ "(@algaSecurity.usuarioAutenticadoIgual(returnObject.cliente.id) or "
			+ "@algaSecurity.gerenciaRestaurante(returnObject.restaurante.id))")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeBuscar { }

	@PreAuthorize("@algaSecurity.podePesquisarPedidos(#filtro.clienteId, #filtro.restauranteId)")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodePesquisar { }

	@PreAuthorize("hasAuthority('SCOPE_WRITE') and isAuthenticated()")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeCriar { }

	@PreAuthorize("@algaSecurity.podeGerenciarPedidos(#codigoPedido)")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeGerenciar { }
    }

    @interface FormasPagamento {

	@PreAuthorize("@algaSecurity.podeConsultarFormasPagamento()")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeConsultar { }

	@PreAuthorize("hasAuthority('SCOPE_WRITE') and hasAuthority('EDITAR_FORMAS_PAGAMENTO')")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeEditar { }
    }

    @interface Cidades {

	@PreAuthorize("@algaSecurity.podeConsultarCidades()")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeConsultar { }

	@PreAuthorize("hasAuthority('SCOPE_WRITE') and hasAuthority('EDITAR_CIDADES')")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeEditar { }
    }

    @interface Estados {

	@PreAuthorize("@algaSecurity.podeConsultarEstados()")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeConsultar { }

	@PreAuthorize("hasAuthority('SCOPE_WRITE') and hasAuthority('EDITAR_ESTADOS')")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeEditar { }
    }

    @interface UsuariosGruposPermissoes {

	@PreAuthorize("hasAuthority('SCOPE_WRITE') and @algaSecurity.usuarioAutenticadoIgual(#filter.usuarioId)")
	@interface PodeAlterarPropriaSenha { }

	@PreAuthorize("hasAuthority('SCOPE_WRITE') and (hasAuthority('EDITAR_USUARIOS_GRUPOS_PERMISSOES') or"
			+ "@algaSecurity.usuarioAutenticadoIgual(#filter.usuarioId))")
	@interface PodeAlterarProprioUsuario { }

	@PreAuthorize("@algaSecurity.podeConsultarUsuariosGruposPermissoes()")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeConsultar { }

	@PreAuthorize("@algaSecurity.podeEditarUsuariosGruposPermissoes()")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeEditar { }
    }

    @interface Estatisticas {

	@PreAuthorize("@algaSecurity.podeConsultarEstatisticas()")
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface PodeConsultar { }
    }
}
