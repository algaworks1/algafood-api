package com.algaworks.algafood.core.security;

import com.algaworks.algafood.core.web.ProfileConstants;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@Profile(ProfileConstants.NOT_EQUALS_TEST)
public class ResourceServerConfig
		extends WebSecurityConfigurerAdapter {

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
	return super.authenticationManager();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
	http
	    	.formLogin().loginPage("/login")
	    .and()
	    	.authorizeRequests()
	    	.antMatchers("/oauth/**").authenticated()
	    .and()
	    	.csrf().disable()
		.cors()
	    .and()
	    	.oauth2ResourceServer()
		.jwt().jwtAuthenticationConverter(jwtAuthenticationConverter());
    }

    private Converter<Jwt, ? extends AbstractAuthenticationToken> jwtAuthenticationConverter() {
	var jwtAuthenticationConverter = new JwtAuthenticationConverter();

	jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwt -> {
	    var authorities = jwt.getClaimAsStringList("authorities");

	    if (authorities == null) {
		authorities = Collections.emptyList();
	    }

	    var jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
	    Collection<GrantedAuthority> grantedAuthorities = jwtGrantedAuthoritiesConverter.convert(jwt);

	    var simpleGrantedAuthorities = authorities.stream()
							.map(SimpleGrantedAuthority::new)
							.collect(Collectors.toList());

	    grantedAuthorities.addAll(simpleGrantedAuthorities);
	    return grantedAuthorities;
	});

	return jwtAuthenticationConverter;
    }
}
