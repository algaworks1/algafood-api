package com.algaworks.algafood.core.security.authorizationserver;

import com.algaworks.algafood.core.web.ProfileConstants;
import com.nimbusds.jose.jwk.JWKSet;
import java.util.Map;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile(ProfileConstants.NOT_EQUALS_TEST)
public class JwkSetController {

    private final JWKSet jwkSet;

    public JwkSetController(JWKSet jwkSet) {
	this.jwkSet = jwkSet;
    }

    @GetMapping("/.well-known/jwks.json")
    public Map<String, Object> keys() {
	return this.jwkSet.toJSONObject();
    }
}
