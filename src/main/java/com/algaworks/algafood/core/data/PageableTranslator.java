package com.algaworks.algafood.core.data;

import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public final class PageableTranslator {

    private PageableTranslator() {
    }

    public static Pageable translate(Pageable apiPageable, Map<String, String> mapeamento) {
	var orders = apiPageable.getSort().get()
				.filter(order -> mapeamento.containsKey(order.getProperty()))
				.map(order -> new Sort.Order(order.getDirection(), mapeamento.get(order.getProperty())))
				.collect(Collectors.toList());

	return PageRequest.of(apiPageable.getPageNumber(), apiPageable.getPageSize(), Sort.by(orders));
    }
}
