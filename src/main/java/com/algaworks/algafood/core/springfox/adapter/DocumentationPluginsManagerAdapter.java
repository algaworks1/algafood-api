package com.algaworks.algafood.core.springfox.adapter;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.plugin.core.PluginRegistry;
import org.springframework.stereotype.Component;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.DefaultsProviderPlugin;
import springfox.documentation.spi.service.ResourceGroupingStrategy;
import springfox.documentation.spi.service.contexts.DocumentationContextBuilder;
import springfox.documentation.spring.web.SpringGroupingStrategy;
import springfox.documentation.spring.web.plugins.DefaultConfiguration;
import springfox.documentation.spring.web.plugins.DocumentationPluginsManager;

@Primary
@Component
public class DocumentationPluginsManagerAdapter extends DocumentationPluginsManager {

    private final PluginRegistry<DefaultsProviderPlugin, DocumentationType>   defaultsProviders;
    private final PluginRegistry<ResourceGroupingStrategy, DocumentationType> resourceGroupingStrategies;

    public DocumentationPluginsManagerAdapter(
		    @Qualifier("defaultsProviderPluginRegistry") PluginRegistry<DefaultsProviderPlugin, DocumentationType> defaultsProviders,
		    @Qualifier("resourceGroupingStrategyRegistry") PluginRegistry<ResourceGroupingStrategy, DocumentationType> resourceGroupingStrategies) {
	this.defaultsProviders = defaultsProviders;
	this.resourceGroupingStrategies = resourceGroupingStrategies;
    }

    @Override
    public DocumentationContextBuilder createContextBuilder(DocumentationType documentationType, DefaultConfiguration defaultConfiguration) {
	return ((DefaultsProviderPlugin) this.defaultsProviders.getPluginOrDefaultFor(documentationType, defaultConfiguration))
					     .create(documentationType).withResourceGroupingStrategy(this.resourceGroupingStrategy(documentationType));
    }

    public ResourceGroupingStrategy resourceGroupingStrategy(DocumentationType documentationType) {
	return resourceGroupingStrategies.getPluginOrDefaultFor(documentationType, new SpringGroupingStrategy());
    }

}
